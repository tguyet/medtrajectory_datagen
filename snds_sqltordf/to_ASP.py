#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generation of ASP facts from the synthetic SNDS database

@author: Thomas Guyet
@date: 28/12/2020
"""

import sqlite3
import pandas as pd

conn = sqlite3.connect("../Generator/snds_8313.db")
###############
###############
###############

#Generation of doctors
"""
* list of doctors that had contributed to at least on medical event
SELECT DISTINCT PFS_EXE_NUM from ER_PRS_F;

* doctors description from the above doctors
SELECT DISTINCT PFS_PFS_NUM, PFS_PRA_SPE, PFS_COD_POS from DA_PRA_R WHERE PFS_PFS_NUM in (SELECT DISTINCT PFS_EXE_NUM from ER_PRS_F) AND PFS_PRA_SPE!=50;
"""

# Run the SQL query
df = pd.read_sql_query("SELECT DISTINCT PFS_PFS_NUM, PFS_PRA_SPE, PFS_COD_POS from DA_PRA_R WHERE PFS_PFS_NUM in (SELECT DISTINCT PFS_EXE_NUM from ER_PRS_F) AND PFS_PRA_SPE!=50;", conn)

def save_doctor(d, fout):
    fout.write("doctor(%s,%s,%s).\n"%(d['PFS_PFS_NUM'], d['PFS_PRA_SPE'], d['PFS_COD_POS']))

with open("doctors.lp","w+") as fout:
    df.apply(lambda x : save_doctor(x,fout), axis=1, result_type=None)


#Generation of pharmacies
"""
* pharmacies
SELECT DISTINCT * from DA_PRA_R WHERE PFS_PRA_SPE=50;

* pharmacies that have actually delivered drugs
SELECT DISTINCT PFS_PFS_NUM from DA_PRA_R WHERE PFS_PFS_NUM in (SELECT DISTINCT PFS_EXE_NUM from ER_PRS_F) AND PFS_PRA_SPE=50;
"""

# Run the SQL query
df = pd.read_sql_query("SELECT DISTINCT PFS_PFS_NUM from DA_PRA_R WHERE PFS_PFS_NUM in (SELECT DISTINCT PFS_EXE_NUM from ER_PRS_F) AND PFS_PRA_SPE=50;", conn)

def save(d, fout):
    fout.write("pharm(%s).\n"%(d['PFS_PFS_NUM']))

with open("pharms.lp","w+") as fout:
    df.apply(lambda x : save(x,fout), axis=1, result_type=None)


# Generation of hospitals
"""
* care institutions are in table BE_IDE_R attribute IDE_ETA_NUM (finess number)
* hospital that have actually been involved in cares are in table T_MCOaaB attribute ETA_NUM (finess number)

-> resulting query: SELECT DISTINCT ETA_NUM from T_MCOaaB WHERE ETA_NUM IN (SELECT IDE_ETA_NUM from BE_IDE_R);
"""

# Run the SQL query
df = pd.read_sql_query("SELECT DISTINCT ETA_NUM from T_MCOaaB WHERE ETA_NUM IN (SELECT IDE_ETA_NUM from BE_IDE_R)", conn)

def save(d, fout):
    fout.write("hosp(%s).\n"%(d['ETA_NUM']))

with open("hosps.lp","w+") as fout:
    df.apply(lambda x : save(x,fout), axis=1, result_type=None)

# Generation of patients
"""
select BEN_NIR_PSA, BEN_SEX_COD, BEN_NAI_ANN, BEN_NAI_MOI, BEN_RES_COM, BEN_RES_DPT from IR_BEN_R;
"""
 
# Run the SQL query
df = pd.read_sql_query("select BEN_NIR_PSA, BEN_SEX_COD, BEN_NAI_ANN, BEN_NAI_MOI, BEN_RES_COM, BEN_RES_DPT from IR_BEN_R;", conn)

patients_nir={}
def save(d, fout):
    pid = len(patients_nir)
    patients_nir[d['BEN_NIR_PSA']]=pid
    fout.write("patient(%s).\n"%(pid))
    fout.write("hasSex(%s,%s).\n"%(pid,d['BEN_SEX_COD']))
    fout.write("hasBirthYear(%s,%s).\n"%(pid,d['BEN_NAI_ANN']))
    fout.write("hasLoc(%s,%s,%s).\n"%(pid,d['BEN_RES_DPT'],d['BEN_RES_COM']))

with open("patients.lp","w+") as fout:
    df.apply(lambda x : save(x,fout), axis=1, result_type=None)   
    

###############
# Generation of medical acts
# Run the SQL query
df = pd.read_sql_query("""SELECT BEN_NIR_PSA, CAM_PRS_IDE, CAM_ACT_COD, EXE_SOI_DTD, EXE_SOI_DTF, PFS_EXE_NUM, PRS_MTT_NUM,PSP_ACT_NAT, PRS_NAT_REF FROM ER_CAM_F JOIN ER_PRS_F ON (
            ER_PRS_F.DCT_ORD_NUM	=	ER_CAM_F.DCT_ORD_NUM AND
            ER_PRS_F.FLX_DIS_DTD	=	ER_CAM_F.FLX_DIS_DTD AND
            ER_PRS_F.FLX_EMT_NUM	=	ER_CAM_F.FLX_EMT_NUM AND
            ER_PRS_F.FLX_EMT_ORD	=	ER_CAM_F.FLX_EMT_ORD AND
            ER_PRS_F.FLX_EMT_TYP	=	ER_CAM_F.FLX_EMT_TYP AND
            ER_PRS_F.FLX_TRT_DTD	=	ER_CAM_F.FLX_TRT_DTD AND
            ER_PRS_F.ORG_CLE_NUM	=	ER_CAM_F.ORG_CLE_NUM AND
            ER_PRS_F.PRS_ORD_NUM	=	ER_CAM_F.PRS_ORD_NUM AND
            ER_PRS_F.REM_TYP_AFF	=	ER_CAM_F.REM_TYP_AFF);""", conn)


act_id=0
def save(d, fout):
    global act_id
    pid=patients_nir[d['BEN_NIR_PSA']]
    fout.write("act(%s).\n"%(act_id))
    fout.write("hasCode(%s,ccam(%s)).\n"%(act_id,d['CAM_PRS_IDE'].lower()))
    fout.write("hasPatient(%s,%s).\n"%(act_id,pid))
    fout.write("hasExec(%s,%s).\n"%(act_id,d['PFS_EXE_NUM']))
    fout.write("hasDate(%s,'%s').\n"%(act_id,d['EXE_SOI_DTD']))
    act_id+=1

with open("acts.lp","w+") as fout:
    df.apply(lambda x : save(x,fout), axis=1, result_type=None)  

###############
# Generation of drugs deliveries
# Run the SQL query

df = pd.read_sql_query("""SELECT BEN_NIR_PSA, PHA_PRS_C13, PHA_ACT_QSN, EXE_SOI_DTD, EXE_SOI_DTF, PFS_EXE_NUM, PRS_MTT_NUM,PSP_ACT_NAT, PRS_NAT_REF FROM ER_PHA_F JOIN ER_PRS_F ON (
            ER_PRS_F.DCT_ORD_NUM	=	ER_PHA_F.DCT_ORD_NUM AND
            ER_PRS_F.FLX_DIS_DTD	=	ER_PHA_F.FLX_DIS_DTD AND
            ER_PRS_F.FLX_EMT_NUM	=	ER_PHA_F.FLX_EMT_NUM AND
            ER_PRS_F.FLX_EMT_ORD	=	ER_PHA_F.FLX_EMT_ORD AND
            ER_PRS_F.FLX_EMT_TYP	=	ER_PHA_F.FLX_EMT_TYP AND
            ER_PRS_F.FLX_TRT_DTD	=	ER_PHA_F.FLX_TRT_DTD AND
            ER_PRS_F.ORG_CLE_NUM	=	ER_PHA_F.ORG_CLE_NUM AND
            ER_PRS_F.PRS_ORD_NUM	=	ER_PHA_F.PRS_ORD_NUM AND
            ER_PRS_F.REM_TYP_AFF	=	ER_PHA_F.REM_TYP_AFF);""", conn)

dd_id=0
def save(d, fout):
    global dd_id
    pid=patients_nir[d['BEN_NIR_PSA']]
    fout.write("drugdel(%s).\n"%(dd_id))
    fout.write("hasCode(%s,cip(%s)).\n"%(dd_id,d['PHA_PRS_C13']))
    fout.write("hasQuantity(%s,%s).\n"%(dd_id,d['PHA_ACT_QSN']))
    fout.write("hasPatient(%s,%s).\n"%(dd_id,pid))
    fout.write("hasExec(%s,%s).\n"%(dd_id,d['PFS_EXE_NUM']))
    fout.write("hasDate(%s,'%s').\n"%(dd_id,d['EXE_SOI_DTD']))
    dd_id+=1

with open("drugs.lp","w+") as fout:
    df.apply(lambda x : save(x,fout), axis=1, result_type=None)  

###############
# Generation of medical visits (côté C)
# Run the SQL query
df = pd.read_sql_query("""SELECT BEN_NIR_PSA, EXE_SOI_DTD, PFS_EXE_NUM FROM ER_PRS_F WHERE PRS_NAT_REF=1111;""", conn)

visit_id=0
def save(d, fout):
    global visit_id
    pid=patients_nir[d['BEN_NIR_PSA']]
    fout.write("visit(%s).\n"%(visit_id))
    fout.write("hasPatient(%s,%s).\n"%(visit_id,pid))
    fout.write("hasExec(%s,%s).\n"%(visit_id,d['PFS_EXE_NUM']))
    fout.write("hasDate(%s,'%s').\n"%(visit_id,d['EXE_SOI_DTD']))
    visit_id+=1

with open("visits.lp","w+") as fout:
    df.apply(lambda x : save(x,fout), axis=1, result_type=None)
    

###############
# Generation of hopital stays
"""

"""
# Run the SQL query
df = pd.read_sql_query("""SELECT T_MCOaaB.ETA_NUM, T_MCOaaB.RSA_NUM, DGN_PAL, DGN_REL, NIR_ANO_17, ENT_DAT, SOR_DAT  FROM T_MCOaaB JOIN T_MCOaaC ON (T_MCOaaB.ETA_NUM=T_MCOaaC.ETA_NUM and T_MCOaaB.RSA_NUM=T_MCOaaC.RSA_NUM);""", conn)

hosp_id=0
def save(d, fout):
    global hosp_id
    pid=patients_nir[d['NIR_ANO_17']]
    fout.write("hosp(%s).\n"%(hosp_id))
    fout.write("hasPatient(%s,%s).\n"%(hosp_id,pid))
    fout.write("hasExec(%s,%s).\n"%(hosp_id,d['ETA_NUM']))
    fout.write("hasDate(%s,'%s','%s').\n"%(hosp_id,d['ENT_DAT'],d['SOR_DAT']))
    fout.write("hasDP(%s,%s).\n"%(hosp_id,d['DGN_PAL'].replace(".","").lower()))
    df = pd.read_sql_query("SELECT ASS_DGN, DGN_ASS_NUM FROM T_MCOaaD where ETA_NUM=%s and RSA_NUM=%s;"%(d['ETA_NUM'],d['RSA_NUM']), conn)
    for r in df.iterrows():
        fout.write("hasDA(%s,%s).\n"%(hosp_id,r[1]['ASS_DGN'].replace(".","").lower()))
    hosp_id+=1

with open("hosps.lp","w+") as fout:
    df.apply(lambda x : save(x,fout), axis=1, result_type=None)
    

###############
###############
###############
conn.close()