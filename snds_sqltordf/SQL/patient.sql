SELECT
    BEN_NIR_PSA as numP,
    BEN_SEX_COD as sex,
    BEN_NAI_MOI as birthDate,
    BEN_DCD_DTE as deathDate,
    BEN_RES_COM as localisation
FROM IR_BEN_R