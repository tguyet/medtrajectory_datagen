SELECT 
	ER_PRS_F.BEN_NIR_PSA as patient,
	ER_PRS_F.EXE_SOI_DTD as dDate,
	--ER_PRS_F.PSE_SPE_COD as speciality, 
	PFS_EXE_NUM as performer,
	PFS_PRE_NUM as prescriber
FROM ER_PRS_F 
INNER JOIN BATCH_P as bb ON bb.num_P = ER_PRS_F.BEN_NIR_PSA
WHERE PRS_NAT_REF=1111

-- select PSE_SPE_COD  from er_prs_f where prs_nat_ref=1111