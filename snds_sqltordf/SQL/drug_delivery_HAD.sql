SELECT 
bb.num_P as numP,
T_HADaaC.EXE_SOI_DTD as beginDate,
T_HADaaC.EXE_SOI_DTF as endDate,

T_HADaaMED.UCD_COD as ucd,
T_HADaaMED.ETA_NUM_EPMSI as finess

FROM T_HADaaC, BATCH_P
LEFT JOIN T_HADaaMED USING(ETA_NUM_EPMSI, RHAD_NUM)

INNER JOIN BATCH_P as bb ON BATCH_P.num_P = T_HADaaC.NIR_ANO_17