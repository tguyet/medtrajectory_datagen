SELECT
    bb.num_P,
    T_HADaaC.EXE_SOI_DTD as beginDate,
    T_HADaaC.EXE_SOI_DTF as endDate,

    T_HADaaA.CCAM_COD as ccamAct,
    T_HADaaA.PHA_COD as phase,
    T_HADaaA.ETA_NUM_EPMSI as perfomer, -- finess
    T_HADaaC.SEJ_NUM as stay

FROM T_HADaaA

INNER JOIN T_HADaaC USING(ETA_NUM_EPMSI, RHAD_NUM)

INNER JOIN BATCH_P as bb ON BATCH_P.num_P = T_HADaaC.NUM_ENQ
--executant
--LEFT JOIN DA_PRA_R ON
--	ER_PRS_F.PFS_EXE_NUM = DA_PRA_R.PFS_PFS_NUM
