SELECT
    bb.num_P as numP,
    EXE_SOI_DTD as beginDate, 
    EXE_SOI_DTF as endDate,

    CAM_PRS_IDE as act, 
    CAM_TRT_PHA as phase,
    --CAM_ACT_COD as acti,
    PSE_SPE_COD as performer,
    PSP_SPE_COD as prescriber
    

    --ER_PRS_F.PFS_EXE_NUM,
	--PFS_PRA_SPE
	
FROM ER_CAM_F

--Prestation
JOIN ER_PRS_F USING (DCT_ORD_NUM, FLX_DIS_DTD, FLX_EMT_NUM, FLX_EMT_ORD, ORG_CLE_NUM, FLX_EMT_TYP, FLX_TRT_DTD, PRS_ORD_NUM, REM_TYP_AFF)

INNER JOIN BATCH_P as bb ON BATCH_P.num_P = ER_PRS_F.BEN_NIR_PSA
--executant
--LEFT JOIN DA_PRA_R ON
--	ER_PRS_F.PFS_EXE_NUM = DA_PRA_R.PFS_PFS_NUM
