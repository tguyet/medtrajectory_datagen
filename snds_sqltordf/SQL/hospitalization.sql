SELECT
    --NIR_ANO_17 : a joindre avec NUM_ENQ
    T_MCOaaC.NIR_ANO_17 as numP,
	T_MCOaaC.EXE_SOI_DTD as beginDate,
	T_MCOaaC.EXE_SOI_DTF as endDate,

    -- résumé gloabl séjour T_MCOaaB
    -- RUM chaque unité T_MCOaaUM
	T_MCOaaB.DGN_PAL as diagP,

    T_MCOaaC.ETA_NUM as finess

FROM T_MCOaaC
LEFT JOIN T_MCOaaB USING (ETA_NUM, RSA_NUM)