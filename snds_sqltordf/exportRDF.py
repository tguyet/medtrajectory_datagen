import sqlite3
import argparse
import os,sys
from math import ceil
from datetime import datetime
## MANQUE : biology

import os
import glob

class FactoryRDF:

    def __init__(self, directory, db, conn):
        self.indexEvent = 1
        self.directory = directory
        self.conn=conn
        self.db=db

        # list of healthcare professionals that have been used during
        # the generation process, and that have to be represented somewhere
        # ps includes phamarcies and hospitals
        # This list must match elements in the DA_PRA_F table
        self.ps=set()
        # Hospital
        self.hosp=set()
    

    def closeGraphs(self):
        for file in glob.glob( self.directory+'*.trig'):

            with open(file, 'a') as trig :
                trig.write("\n}")

    def generate_CareProfesionals(self):
        if len(self.ps)==0:
            return
        
        qr="""SELECT PFS_PFS_NUM, PFS_PRA_SPE, PFS_FIN_NUM FROM DA_PRA_R where PFS_PFS_NUM IN ("""
        for id in self.ps:
            if id != None :
                qr += "'"+str(id)+"',"
        qr = qr[:-1]+ ')'

        cur = self.conn.cursor()
        cur.execute(qr)
        listps = cur.fetchall()
        cur.close()
        

        qr="""SELECT PFS_SPE_COD FROM IR_SPE_D"""
        cur = self.conn.cursor()
        cur.execute(qr)
        listspe = cur.fetchall()
        cur.close()

        #remove 0/99 (non renseigne), 36 (dentistes), 1 (generaliste), 
        listspe=[x[0] for x in listspe]
        listspe.remove(0)
        listspe.remove(99)
        listspe.remove(36)
        listspe.remove(1)


        strps="\n\n"
        for ps in listps:
            if ps[1]=='50':
                strps+=f"db:PS{ps[0]} rdf:instanceOf snds:Pharmacy. \n"
            elif ps[1]=='1':
                strps+=f"db:PS{ps[0]} rdf:instanceOf snds:General_Practitioner. \n"
            elif int(ps[1]) in listspe:
                strps+=f"""db:PS{ps[0]} rdf:instanceOf snds:Health_Professional;
                                    snds:has_speciality {ps[1]}. \n"""
            else:
                strps+=f"db:PS{ps[0]} rdf:instanceOf snds:Care_Provider. \n"

        for h in self.hosp:
            strps+=f"db:Hosp{h} rdf:instanceOf snds:Hospital. \n"

        with open(self.directory+'care_providers.ttl', 'a') as trig :
            trig.write(self.generatePrefixes())
            trig.write(strps)

        return strps
    
    def addVisit(self,patient, dateB, perf, prov):
        #event_i = str(patient) + "EVT" + str(self.indexEvent)
        event_i = "EVT" + str(self.indexEvent)

        
        try:
            dateB = datetime.strptime(dateB,'%Y-%m-%d %H:%M:%S.%f').strftime("%Y-%m-%d")
        except ValueError:
            pass

        event_tuple = f"""
        :pat{patient} snds:has_event :{event_i}.
        :{event_i} rdf:instanceOf snds:Visit;
        snds:has_time :{event_i}_time;
        snds:has_prescriber db:PS{prov};
        snds:has_performer db:PS{perf}.
        :{event_i}_time rdf:instanceOf time:Instant;
        time:has_beginning "{dateB}"^^xsd:date.
        """
        #add healthcare professional
        self.ps.add(perf)
        self.ps.add(prov)

        self.indexEvent = self.indexEvent + 1
        return event_tuple

    def addHospitalisation(self, patient, dateB, dateF, CIMs, finess):
        #event_i = str(patient) + "EVT" + str(self.indexEvent)
        event_i = "EVT" + str(self.indexEvent)

        dateB = datetime.strptime(dateB,'%Y-%m-%d %H:%M:%S.%f').strftime("%Y-%m-%d")
        dateF = datetime.strptime(dateF,'%Y-%m-%d %H:%M:%S.%f').strftime("%Y-%m-%d")


        if len(CIMs)==4:
            CIMs=CIMs[0:3]+"."+CIMs[3]

        event_tuple = f"""
        :pat{patient} snds:has_event :{event_i}.
        :{event_i} rdf:instanceOf snds:ShortStay;
        snds:diag_principal cim:{CIMs};
        snds:has_time :{event_i}_time;
        snds:has_performer db:Hosp{finess}.
        :{event_i}_time rdf:instanceOf time:Interval;
        time:has_beginning "{dateB}"^^xsd:date;
        time:has_end "{dateF}"^^xsd:date.
        """
        #add healthcare professional
        self.hosp.add(finess)

        self.indexEvent = self.indexEvent + 1
        return event_tuple


    def addDrugDelivery(self, patient, dateB, cip13, qty, perf, prov, atc, generic, doses):
        #event_i = str(patient) + "EVT" + str(self.indexEvent)
        event_i = "EVT" + str(self.indexEvent)

        try:
            dateB = datetime.strptime(dateB,'%Y-%m-%d %H:%M:%S.%f').strftime("%Y-%m-%d")
        except ValueError:
            pass

        event_tuple = f"""
        :pat{patient} snds:has_event :{event_i}.
        :{event_i} rdf:instanceOf snds:DrugDelivery;
        snds:has_time :{event_i}_time;
        snds:has_cip "{cip13}";
        snds:has_quantity {qty};
        snds:has_prescriber db:PS{prov};
        snds:has_performer db:PS{perf};
        snds:deliver atc:{atc}.
        :{event_i}_time rdf:instanceOf time:Instant;
        time:has_beginning "{dateB}"^^xsd:date.
        """
        #add healthcare professional
        self.ps.add(perf)
        self.ps.add(prov)

        self.indexEvent = self.indexEvent + 1
        return event_tuple

    def addALD(self, patient, CIM_MTF, dateB, dateF): 
        #event_i = str(patient) + "EVT" + str(self.indexEvent)
        event_i = "EVT" + str(self.indexEvent)

        try:
            dateB = datetime.strptime(dateB,'%Y-%m-%d %H:%M:%S.%f').strftime("%Y-%m-%d")
        except ValueError:
            pass

        if len(CIM_MTF)==4:
            CIM_MTF=CIM_MTF[0:3]+"."+CIM_MTF[3]

        event_tuple = f"""
        :pat{patient} snds:has_event :{event_i}.
        :{event_i} rdf:instanceOf snds:ALDExposure;
        snds:has_time :{event_i}_time;
        snds:has_cim cim:{CIM_MTF}.
        :{event_i}_time rdf:instanceOf time:Interval;
        time:has_beginning "{dateB}"^^xsd:date.
        """

        try:
            dateF = datetime.strptime(dateF,'%Y-%m-%d %H:%M:%S.%f').strftime("%Y-%m-%d")
        except ValueError:
            pass
        if dateF!="2100-01-01":
            event_tuple += f""":{event_i}_time time:has_end "{dateF}"^^xsd:date."""

        self.indexEvent = self.indexEvent + 1
        return event_tuple

    def addPatient(self, idpatient, gender, birthdate, deathdate =None): 
        pat = f"""\n\nGRAPH :{idpatient}{{
        :pat{idpatient} rdf:instanceOf snds:Patient;
        snds:has_sex {gender} ;
        snds:has_birthdate "{birthdate}"^^xsd:date.
        """
        if deathdate is not None:
            pat += f':{idpatient} snds:has_deathdate "{birthdate}"^^xsd:date.'

        return pat

    def generatePrefixes(self):
        prefix = "@prefix {}: <{}>.\n"
        prefixes = ''
        
        prefixes += prefix.format('xsd', 'http://www.w3.org/2001/XMLSchema#')
        prefixes += prefix.format('rdf', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#')
        prefixes += prefix.format('rdfs', 'http://www.w3.org/2000/01/rdf-schema#')
        prefixes += prefix.format('owl', 'http://www.w3.org/2002/07/owl#')
        prefixes += prefix.format('atc', 'http://purl.bioontology.org/ontology/ATC/')
        prefixes += prefix.format('time', 'http://www.w3.org/2006/time#')
        prefixes += prefix.format('snds', 'http://localhost/ontologies/snds#')
        prefixes += prefix.format('db', 'http://localhost/ontologies/db/')
        prefixes += prefix.format('cim', 'http://chu-rouen.fr/cismef/CIM-10#')
        prefixes += prefix.format('', 'http://localhost/ontologies/')
        prefixes += "@base <http://localhost/ontologies/>.\n"
        return prefixes


    def view_creation(self, s_id):
        self.db.execute("DROP VIEW IF EXISTS BATCH_P;")
        self.conn.commit()

        if len(s_id)==0:
            return
        view = """CREATE VIEW BATCH_P AS
        SELECT 
        IR_BEN_R.BEN_NIR_PSA as num_P
        FROM IR_BEN_R where IR_BEN_R.BEN_NIR_PSA IN ("""
        for id in s_id :
            if id != None :
                view += "'"+id+"',"
        view = view[:-1]+ ')'

        
        self.db.execute(view)
        self.conn.commit()



    def create_graphs(self, batch=1, unique_graph=True):

        self.queries_loc="./snds_sqltordf/SQL/"

        query = open( os.path.join(self.queries_loc,'patient.sql'), 'r').read()
        self.db.execute(query)
        patients = self.db.fetchall()
        nb_p = len(patients)
        batch_begin = 0
        per_batch = ceil(nb_p/batch)
        batch_end = per_batch

        #we build batchs of patients
        for l in range(0,batch):
            print("BATCH ",l, ": ")
            
            if not unique_graph:
                trigs={}

            # Sélection des patients du batch ---> création vue
            patients_id =[p[0] for p in patients[batch_begin: batch_end]]
            factory.view_creation(patients_id)

            for p in patients[batch_begin: batch_end]:
                #create a patient instance with properties and a sequence instance
                if p[3]=="2100-01-01":
                    pat=self.addPatient(p[0], p[1], p[2])
                else:
                    pat=self.addPatient(p[0], p[1], p[2], p[3])

                if unique_graph:
                    with open(self.directory+str(p[0])+'.trig', 'a') as trig :
                        trig.write(self.generatePrefixes())
                        trig.write(pat)
                else:
                    trigs[p[0]]=pat


            queries= [('drug_delivery.sql',self.addDrugDelivery),
                      ('visit.sql',self.addVisit),
                      ('hospitalization.sql',self.addHospitalisation),
                      ('ALD.sql',self.addALD)]

            for query_str, query_handler in queries:
            
                query = open(os.path.join(self.queries_loc,query_str), 'r').read()
                try:
                    rows = self.db.execute(query)
                    print("Execute "+query_str)
                except:
                    rows=[]

                for row in rows:
                        #try:
                        tuples=query_handler(*row)
                        if unique_graph:
                            #faire un test --> si pas là, lever une exception
                            if not os.path.exists(self.directory+str(row[0])+'.trig'):
                                print("File does not exists:" + self.directory+str(row[0])+'.trig')
                                
                            with open(self.directory+str(row[0])+'.trig', 'a') as trig :
                                trig.write(tuples)
                        else:
                            trigs[row[0]] += tuples
                        #except:
                        #    print("\tError in row")
            if not unique_graph:
                trig = open(self.directory+"batch_"+str(l)+'.trig', 'a')
                trig.write(self.generatePrefixes())
                for _,v in trigs.items():
                    trig.write(v)
                    trig.write("\n}")

            batch_begin = batch_end
            batch_end = min(batch_end+per_batch, nb_p)

        self.generate_CareProfesionals()
        if unique_graph:
            self.closeGraphs()


if __name__ == "__main__":
    #Création argument
    parser = argparse.ArgumentParser()
    #parser.add_argument("-db", "--database", dest="file_db", help="file SQlite", required=True)
    args = parser.parse_args()
    batch = 1

    args.file_db = "./AVCGenerator/snds_testgen.db"

    if not os.path.isfile(args.file_db):
        print("File doesn't exist")
        sys.exit()

    dd = datetime.now().strftime('%Y%m%d_%H%M%S')
    db_output = './outputs_'+dd+'/'
    if not os.path.exists(db_output):
        os.makedirs(db_output)
    else :
        print(f"Database {db_output} already exists")
        sys.exit()
    
    conn = sqlite3.connect(args.file_db)
    db = conn.cursor()
    
    factory = FactoryRDF(db_output, db, conn)
    factory.create_graphs(unique_graph=False, batch=1)

    db.close()
    