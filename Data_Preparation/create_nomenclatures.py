#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sqlalchemy as sa
import os
from tableschema import Table
import glob
import pandas as pd
import json

rootdir= os.getcwd()

rootschema  = "./schema-snds/schemas"
rootsnomencl= "./schema-snds/nomenclatures"
db_output_name ="snds_nomenclature.db"


print("Schema cleaning'")
#Suppression de la date vide !
dataset=pd.read_csv( os.path.join(rootsnomencl,"ORAVAL/IR_DTE_V.csv"), sep=";")
dataset.dropna(inplace=True)
dataset.to_csv( os.path.join(rootsnomencl,"ORAVAL/IR_DTE_V.csv"), sep=";", index=False )


# discard IR_MIR_D.json a,d IR_MIR_V.json -> problem with some entries having too long integers for SQLite !
try:
    os.rename(os.path.join(rootsnomencl,"ORAVAL/IR_MIR_D.json"),  os.path.join(rootsnomencl,"ORAVAL/IR_MIR_D.json.ignore"))
    os.rename(os.path.join(rootsnomencl,"ORAVAL/IR_MIR_V.json"),  os.path.join(rootsnomencl,"ORAVAL/IR_MIR_V.json.ignore"))
except FileNotFoundError:
    None

print("Create database: '" + os.path.join(rootdir,db_output_name) + "'")
# Create SQL database
#db = sa.create_engine('sqlite://') #save in memory
db = sa.create_engine('sqlite:///'+os.path.join(rootdir,db_output_name))

print("### Chargement détaillé des nomenclatures (avec valeurs) ###")
#Load each of the nomenclature 
print("=> ORAVAL")
for fjson in glob.glob(rootsnomencl+"/ORAVAL/*.json"):
	fcsv = fjson[:-4]+"csv"
	table=Table(fcsv, schema=fjson)
	print("\t* load table '" +str(table.schema.descriptor['name'])+"': "+str(table.schema.descriptor.setdefault('title', "---")) )
	table.save(table.schema.descriptor['name'], storage='sql', engine=db)

print("=> DREES")
for fjson in glob.glob(rootsnomencl+"/DREES/*.json"):
	fcsv = fjson[:-4]+"csv"
	table=Table(fcsv, schema=fjson)
	print("\t* load table '" +str(table.schema.descriptor['name'])+"': "+str(table.schema.descriptor.setdefault('title', "---")) )
	table.save(table.schema.descriptor['name'], storage='sql', engine=db)

print("=> ORAREF")
for fjson in glob.glob(rootsnomencl+"/ORAREF/*.json"):
	fcsv = fjson[:-4]+"csv"
	table=Table(fcsv, schema=fjson)
	print("\t* load table '" +str(table.schema.descriptor['name'])+"': "+str(table.schema.descriptor.setdefault('title', "---")) )
	table.save(table.schema.descriptor['name'], storage='sql', engine=db)

print("=> ATIH")
for fjson in glob.glob(rootsnomencl+"/ATIH/*.json"):
	fcsv = fjson[:-4]+"csv"
	table=Table(fcsv, schema=fjson)
	print("\t* load table '" +str(table.schema.descriptor['name'])+"': "+str(table.schema.descriptor.setdefault('title', "---")) )
	table.save(table.schema.descriptor['name'], storage='sql', engine=db)

### Load the database schema ###
## -> the order in which the different parts of the schema are loaded is very important (foreign key dependencies)