import pandas as pd
import json

rootsnomencl = "./schema-snds/schemas"


with open(rootsnomencl + "/DCIR/ER_PRS_F.json", "r") as read_file:
    data = json.load(read_file)

for f in data["fields"]:
    if (
        f["name"] == "PFS_EXE_NUM"
        or f["name"] == "PFS_PRE_NUM"
        or f["name"] == "PRS_MTT_NUM"
        or f["name"] == "PSP_PPS_NUM"
        or f["name"] == "ETB_PRE_FIN"
    ):
        try:
            del f["constraints"]
        except:
            continue

with open(rootsnomencl + "/DCIR/ER_PRS_F.json", "w") as write_file:
    json.dump(data, write_file, indent=2)


# add primary key to ER_PHA_F

with open(rootsnomencl + "/DCIR/ER_PHA_F.json", "r") as read_file:
    data = json.load(read_file)

data["primaryKey"] = [
    "DCT_ORD_NUM",
    "FLX_DIS_DTD",
    "FLX_EMT_NUM",
    "FLX_EMT_ORD",
    "FLX_EMT_TYP",
    "FLX_TRT_DTD",
    "ORG_CLE_NUM",
    "PRS_ORD_NUM",
    "REM_TYP_AFF",
]

with open(rootsnomencl + "/DCIR/ER_PHA_F.json", "w") as write_file:
    json.dump(data, write_file, indent=2)

# add primary key to ER_CAM_F
with open(rootsnomencl + "/DCIR/ER_CAM_F.json", "r") as read_file:
    data = json.load(read_file)

data["primaryKey"] = [
    "DCT_ORD_NUM",
    "FLX_DIS_DTD",
    "FLX_EMT_NUM",
    "FLX_EMT_ORD",
    "FLX_EMT_TYP",
    "FLX_TRT_DTD",
    "ORG_CLE_NUM",
    "PRS_ORD_NUM",
    "REM_TYP_AFF",
]

with open(rootsnomencl + "/DCIR/ER_CAM_F.json", "w") as write_file:
    json.dump(data, write_file, indent=2)


# add primary key to T_MCOaaD
with open(rootsnomencl + "/PMSI/PMSI MCO/T_MCOaaD.json", "r") as read_file:
    data = json.load(read_file)

data["primaryKey"] = ["ETA_NUM", "RSA_NUM", "DGN_ASS_NUM"]

with open(rootsnomencl + "/PMSI/PMSI MCO/T_MCOaaD.json", "w") as write_file:
    json.dump(data, write_file, indent=2)

# wrong format for ETA_NUM to T_MCOaaB
with open(rootsnomencl + "/PMSI/PMSI MCO/T_MCOaaB.json", "r") as read_file:
    data = json.load(read_file)
for f in data["fields"]:
    if f["name"] == "ETA_NUM":
        # modify the type
        f["type"] = "string"
        # remove the cosntraints (for integers)
        try:
            del f["constraints"]
        except:
            continue
with open(rootsnomencl + "/PMSI/PMSI MCO/T_MCOaaB.json", "w") as write_file:
    json.dump(data, write_file, indent=2)


# wrong format for ETA_NUM to T_MCOaaB
with open(rootsnomencl + "/PMSI/PMSI MCO/T_MCOaaC.json", "r") as read_file:
    data = json.load(read_file)
for f in data["fields"]:
    if f["name"] == "ETA_NUM":
        # modify the type
        f["type"] = "string"
        # remove the cosntraints (for integers)
        try:
            del f["constraints"]
        except:
            continue
with open(rootsnomencl + "/PMSI/PMSI MCO/T_MCOaaC.json", "w") as write_file:
    json.dump(data, write_file, indent=2)


# add primary key to KI_CCI_R
with open(rootsnomencl + "/Causes de décès/KI_CCI_R.json", "r") as read_file:
    data = json.load(read_file)

data["primaryKey"] = ["DCD_IDT_ENC"]

with open(rootsnomencl + "/Causes de décès/KI_CCI_R.json", "w") as write_file:
    json.dump(data, write_file, indent=2)


# add primary key to KI_ECD_R
with open(rootsnomencl + "/Causes de décès/KI_ECD_R.json", "r") as read_file:
    data = json.load(read_file)

data["primaryKey"] = ["DCD_IDT_ENC"]

with open(rootsnomencl + "/Causes de décès/KI_ECD_R.json", "w") as write_file:
    json.dump(data, write_file, indent=2)


# add primary key to T_MCOaaUM
with open(rootsnomencl + "/PMSI/PMSI MCO/T_MCOaaUM.json", "r") as read_file:
    data = json.load(read_file)

data["primaryKey"] = ["ETA_NUM", "RSA_NUM", "UM_ORD_NUM", "DGN_PAL", "DGN_REL"]

with open(rootsnomencl + "/PMSI/PMSI MCO/T_MCOaaUM.json", "w") as write_file:
    json.dump(data, write_file, indent=2)


# add primary key to T_MCOaaA
with open(rootsnomencl + "/PMSI/PMSI MCO/T_MCOaaA.json", "r") as read_file:
    data = json.load(read_file)

data["primaryKey"] = ["ETA_NUM", "RSA_NUM", "RUM_SEQ", "CDC_ACT"]

with open(rootsnomencl + "/PMSI/PMSI MCO/T_MCOaaA.json", "w") as write_file:
    json.dump(data, write_file, indent=2)


# add primary key to IR_IMB_R
with open(rootsnomencl + "/DCIR_DCIRS/IR_IMB_R.json", "r") as read_file:
    data = json.load(read_file)

data["primaryKey"] = ["BEN_NIR_PSA", "BEN_RNG_GEM"]

with open(rootsnomencl + "/DCIR_DCIRS/IR_IMB_R.json", "w") as write_file:
    json.dump(data, write_file, indent=2)

# add primary key to T_MCOaaMED
with open(rootsnomencl + "/PMSI/PMSI MCO/T_MCOaaMED.json", "r") as read_file:
    data = json.load(read_file)

data["primaryKey"] = ["ETA_NUM", "RSA_NUM", "UCD_COD"]

with open(rootsnomencl + "/PMSI/PMSI MCO/T_MCOaaMED.json", "w") as write_file:
    json.dump(data, write_file, indent=2)
