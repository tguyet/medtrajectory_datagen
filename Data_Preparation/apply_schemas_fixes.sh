#!/usr/bash

SDNSSCHEMA=snds-schema

## we ignore these tables that are not used and that generate errors with sqlite due to too large values
mv $SDNSSCHEMA/nomenclatures/ORAVAL/IR_MIR_D.json $SDNSSCHEMA/nomenclatures/ORAVAL/IR_MIR_D.json.ignore
mv $SDNSSCHEMA/nomenclatures/ORAVAL/IR_MIR_V.json $SDNSSCHEMA/nomenclatures/ORAVAL/IR_MIR_V.json.ignore
