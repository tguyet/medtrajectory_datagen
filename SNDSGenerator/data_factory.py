#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#![allow(non_snake_case)]

"""
Basic random data SNDS data simulator in our own data model.
This file contains a list of Factory classes that yield coherent instances of the classes in our data model.

@author: Thomas Guyet
@date: 08/2020
"""

from .database_model import (
    Patient,
    ALD,
    GP,
    Specialist,
    Provider,
    DrugDelivery,
    Etablissement,
    MedicalVisit,
    MedicalAct,
    ShortHospStay,
)
from math import isnan
import numpy.random as rd
import pandas as pd
import numpy as np
import sqlite3  ##Here, I use sqlite to query the database (ie to access nomenclatures)
from itertools import chain

from datetime import timedelta, date


class FactoryContext:
    """
    class FactoryContext

    The class contains usefull tools to be used by different factories.
    -> a connexion to the database which enables to access the values that are used for some attributes of the database

    Attributes:
    -----------
    conn: a database connection
    codes_geo: a collection of geographical codes (administrative areas or cities), comes from IR_GEO_V
    year: a reference year to make the simulation
    """

    def __init__(self, nomenclatures="snds_nomenclature.db"):
        self.conn = sqlite3.connect(
            nomenclatures
        )  # connexion to the nomenclature database
        self.codes_geo = None
        self.year = 2021

    def __del__(self):
        if hasattr(self, "conn") and self.conn:
            self.conn.close()

    def generate_date(self, begin=date(1900, 1, 1), end=date(2020, 1, 1)):
        """
        Generate a random date between two dates

        TODO: define the notion of constraint for temporal constraintes
        """
        delta = timedelta(days=rd.randint(1, (end - begin).days + 1))
        return begin + delta

    def generate_location(self):
        """
        return a couple with a random city code and corresponding dpt code
        """
        if self.codes_geo is None:
            cur = self.conn.cursor()
            cur.execute("select GEO_DPT_COD, GEO_COM_COD from IR_GEO_V ;")
            self.codes_geo = cur.fetchall()
            cur.close()
        return self.codes_geo[rd.randint(len(self.codes_geo))]

    def __genDpt2__(self):
        """
        generate a list of depts codes
        """
        dpts = ["{:02}".format(i) for i in range(1, 98)]
        dpts.append("2A")
        dpts.append("2B")
        dpts.append("99")
        return rd.choice(dpts)


class PharmacyFactory:
    def __init__(self, context: FactoryContext):
        self.context = context

    def generate(self, n):
        """
        Generate n pharmacies including half GPs and half specialists
        """
        Pharmacies = []
        for i in range(n):
            p = Provider()
            p.dpt = self.context.__genDpt2__()
            # p.cat_nat=50 #pharmacie de ville
            p.speciality = 50  # pharmacie de ville
            p.id = str(p.dpt) + str(p.catpro) + "{:05}".format(rd.randint(99999))
            Pharmacies.append(p)
        return Pharmacies


class PhysicianFactory:
    current_id = 0

    def __init__(self, con):
        self.context = con
        PhysicianFactory.current_id = 0

    def __generatePSNUM__(self, p):
        """
        generate a PSNUM that respect "https://documentation-snds.health-data-hub.fr/fiches/professionnel_sante.html"
        - 2 characters with dpt
        - 1 character with category
        - 5 random numbers
        """

        # return str(p.dpt)+str(p.catpro)+"{:05}".format(rd.randint(99999))
        PhysicianFactory.current_id += 1
        return str(p.dpt) + str(p.catpro) + "{:05}".format(PhysicianFactory.current_id)

    def generateGP(self, n):
        """
        Generate n General Practitioners
        """
        physicians = []
        for i in range(n):
            p = GP()
            p.dpt = self.context.__genDpt2__()
            p.id = self.__generatePSNUM__(p)
            physicians.append(p)
        return physicians

    def generateSpecialists(self, n):
        """
        Generate n specialists (with random specialities)
        """

        # First get the list of specialities
        cur = self.context.conn.cursor()
        cur.execute(
            "select PFS_SPE_COD from IR_SPE_V where PFS_SPE_COD>1 and PFS_SPE_COD<=85;"
        )
        codes_spe = cur.fetchall()
        cur.close()
        codes_spe = [c[0] for c in codes_spe]

        physicians = []
        for i in range(n):
            p = Specialist()
            p.dpt = self.context.__genDpt2__()
            p.speciality = rd.choice(codes_spe)  # random choice of a speciality
            p.id = self.__generatePSNUM__(p)
            physicians.append(p)
        return physicians

    def generate(self, n):
        """
        Generate n physicians including half GPs and half specialists
        """
        physicians = self.generateGP(int(n / 2))
        physicians += self.generateSpecialists(int(n / 2))
        return physicians


class PatientFactory:
    """

    attributes:
    """

    def __init__(self, con, GPs=None):
        """
        - con is a Factory context, which include a connexion to the nomenclature database
        - GPs is a list of General practitioners or a list labels
        """
        self.context = con
        self.GPs = GPs         

        cur = self.context.conn.cursor()
        cur.execute(
            "select CIM_COD, ALD_030_COD from IR_CIM_V where ALD_030_COD>=1 and ALD_030_COD<=30;"
        )
        self.aldcodes = cur.fetchall()
        cur.close()

    def __generateNIR__(self, p):
        """
        generate the NIR of the patient
        """
        birth_location = self.context.generate_location()
        p.NIR = (
            str(p.Sex)
            + "{:02}".format(p.BD.month)
            + "{:02}".format(p.BD.year % 100)
            + birth_location[0]
            + birth_location[1]
            + "{:06}".format(rd.randint(999999))
        )

    def generate(self, n):
        """
        - n number of patients to generate
        """
        patients = []
        for i in range(n):
            p = Patient()
            p.Sex = rd.choice([1, 2, 9], p=[0.495, 0.495, 0.01])
            p.BD = self.context.generate_date()
            p.Dpt, p.City = self.context.generate_location()
            self.__generateNIR__(p)

            if self.GPs:
                mtt = rd.choice(self.GPs)
                """
                if isinstance(mtt, GP):
                    p.MTT=mtt.id
                elif isinstance(mtt, str):
                    p.MTT=mtt
                """
                p.MTT = mtt
            else:
                p.MTT = None

            patients.append(p)

        return patients

    def __load__(self, patient, colmap):
        p = Patient()
        p.raw_data = patient.copy()
        if "sex" in colmap:
            p.Sex = patient[colmap["sex"]]
        else:
            p.Sex = rd.choice([1, 2, 9], p=[0.495, 0.495, 0.01])

        if "age" in colmap:
            age = int(patient[colmap["age"]])
            p.BD = self.context.generate_date(
                date(self.context.year - age, 1, 1),
                date(self.context.year - age, 12, 31),
            )
        else:
            p.BD = self.context.generate_date()

        if ("city_code" in colmap) and ("dpt_code" in colmap):
            p.Dpt, p.City = patient[colmap["dpt_code"]], patient[colmap["city_code"]]
        else:
            p.Dpt, p.City = self.context.generate_location()

        if "NIR" in colmap:
            p.NIR = patient[colmap["NIR"]]
            p.__internal_id__ = p.NIR
        else:
            self.__generateNIR__(p)
            p.__internal_id__ = patient.name

        if self.GPs:
            mtt = rd.choice(self.GPs)
            p.MTT = mtt
        else:
            p.MTT = None

        if "dc_date" in colmap and "dc_cause" in colmap:
            if not pd.isnull(patient[colmap["dc_date"]]):
                # print(patient[colmap["dc_date"]], patient[colmap["dc_cause"]])
                p.DeathDate = patient[colmap["dc_date"]]
                p.CauseDC = patient[colmap["dc_cause"]]

        if (
            "ald_cimcode" in colmap
            and "ald_date" in colmap
            and str(patient[colmap["ald_cimcode"]]) != "nan"
            and str(patient[colmap["ald_cimcode"]]) != "None"
        ):
            # ald.aldcode = aldcodes[ ]
            ald_num = list(
                filter(lambda a: a[0] == patient[colmap["ald_cimcode"]], self.aldcodes)
            )
            if len(ald_num) > 0:
                ald = ALD()
                ald.motif = patient[colmap["ald_cimcode"]]
                ald.aldcode = ald_num[0][1]
                ald.start = patient[colmap["ald_date"]]
                p.ALD.append(ald)

        return p

    def load(self, df, colmap, drop_errors=False):
        """
        - df pandas dataframe containing a columwise description of patients.
            This will be the basis to create new instances of patients
        - colmap a map that gives the column name in which the attributes of a
            patient have to be found.
            The possible attributes to define from columns are: 'sex', 'age', 'NIR', ('city_code' and 'dpt_code' together)
        - drop_errors if True, the instances

        The function ensures some validity checking:  [TODO]
            - some attributes are mandatory
            - the attributes are casted to the right type (otherwise errors are thrown)
            - some attribute constraints (such as string lengths)

        Note that if the NIR attribute is not provide, the index of a row in the
        dataframe `df` is used as an internal index that will be used for joining
        the

        returns a collection of Patient objects
        """
        return df.apply(self.__load__, colmap=colmap, axis=1).to_list()


class DrugsDeliveryFactory:
    def __init__(self, con, Pharmacies):
        """
        - con is a Factory context, which include a connexion to the nomenclature database
        - Pharmacies is a list of Pharmacies that can deliver drugs
        """
        self.context = con
        self.Pharmacies = Pharmacies

        # First get the list of drugs (CIP13 codes)
        cur = self.context.conn.cursor()
        cur.execute("select PHA_CIP_C13 from IR_PHA_R;")
        self.cips = cur.fetchall()
        cur.close()
        self.cips = [c[0] for c in self.cips]

    def generate_one(self, p):
        """
        Generate a single drug delivery for a patient p
        The delivery is added to the patient it self

        - p: a patient to which generate a drug delivery
        """

        # generate a drug delivery with a random CIP
        cip13 = rd.choice(self.cips)
        dd = DrugDelivery(cip13, p, rd.choice(self.Pharmacies))

        dd.date_debut = self.context.generate_date(begin=p.BD, end=date(2020, 1, 1))
        dd.date_fin = dd.date_debut

        p.drugdeliveries.append(dd)

    def generate(self, p, maxdd=50):
        """
        Generate a drug delivery for a patient p
        The delivery is added to the patient itself

        - p: a patient to which generate a drug delivery
        - maxdd: maximal number of deliveries
        """
        for i in range(rd.randint(maxdd)):
            self.generate_one(p)

    def __load__(self, dd, p, colmap):
        cip13 = dd[colmap["cip"]]
        drug = DrugDelivery(cip13, p, rd.choice(self.Pharmacies))
        if "date" in colmap:
            drug.date_debut = dd[colmap["date"]]
        return drug

    def load(self, p: Patient, df, colmap, drop_errors=False) -> None:
        """
        - df pandas dataframe containing a columwise description of a drug delivery.
            This data frame must at least contain an attribute with the patient
            id and an attribute with the cip code.
        - colmap a map that gives the column name in which the attributes of a
            patient have to be found.
            The mandatory attributes to define from columns are: 'cip'
            The possible attributes to define from columns are: date'

            In addition, the attribute 'idp' must define the column corresponding
            to the patient identifier (attribute containing the value of the index
            in the dataframe of patients, while patient was loaded and that is used
            as internal identifier, not necessarily the NIR)

        - drop_errors if True, the instances [TODO]

        The function ensures some validity checking:  [TODO]
            - some attributes are mandatory
            - the attributes are casted to the right type (otherwise errors are thrown)
            - some attribute constraints (such as string lengths)

        The function modifies the patient p instance
        """

        if not drop_errors and (not 'pid' in colmap or not 'cip' in colmap):
            raise ValueError("expected definition of 'pid' and 'cip' attributes in colmap")
        drugs = df[df[colmap["pid"]] == p.__internal_id__].apply(
            self.__load__, p=p, colmap=colmap, axis=1
        )
        if len(drugs) != 0:
            p.drugdeliveries.extend(drugs)

    def load_allpatients(self, patients, df, colmap, drop_errors=False):
        # map(lambda p: self.load(p, df, colmap, drop_errors), patients)
        # list(chain.from_iterable(map(lambda p: drugfact.load(p, drugs, drugs_attributes), patients)))
        # return list(chain.from_iterable(map(lambda p: self.load(p, df, colmap, drop_errors), patients)))
        list(map(lambda p: self.load(p, df, colmap, drop_errors), patients))


class EtablissementFactory:
    def __init__(self, con):
        self.context = con

    def generate(self):
        return Etablissement()


class VisitFactory:
    def __init__(self, con, physicians):
        self.context = con
        self.physicians = physicians

    def generate_one(self, p):
        """
        - p patient to which
        """
        visit = MedicalVisit(p, p.MTT)

        visit.date_debut = self.context.generate_date(begin=p.BD, end=date(2020, 1, 1))
        visit.date_fin = visit.date_debut

        p.visits.append(visit)

    def generate(self, p, nbs=30):
        for i in range(nbs):
            self.generate_one(p)


class ActFactory:
    def __init__(self, con, physicians):
        self.context = con
        self.physicians = physicians

        cur = self.context.conn.cursor()
        cur.execute("SELECT CAM_PRS_IDE_COD FROM IR_CCAM_V54;")
        ccams = cur.fetchall()
        cur.close()
        self.ccams = [c[0] for c in ccams]

    def generate_one(self, p):
        """
        - p patient
        """
        ccam = rd.choice(self.ccams)
        spe = rd.choice(self.physicians)
        mact = MedicalAct(ccam, p, spe)
        mact.date_debut = self.context.generate_date(begin=p.BD, end=date(2020, 1, 1))
        mact.date_fin = mact.date_debut

        p.medicalacts.append(mact)

    def generate(self, p, nbs=30):
        """
        - p patient
        - nbs number of act to generate for a patient
        """
        for i in range(nbs):
            self.generate_one(p)


class ShortStayFactory:
    def __init__(self, con, hospitals):
        self.context = con

        # get the list of CIM codes
        cur = self.context.conn.cursor()
        cur.execute("SELECT CIM_COD FROM IR_CIM_V;")
        cims = cur.fetchall()
        cur.close()
        self.cims = [c[0] for c in cims]

        self.hospitals = hospitals

        """
        cur = self.context.conn.cursor()
        cur.execute("SELECT CAM_PRS_IDE_COD FROM IR_CCAM_V54;")
        ccams = cur.fetchall()
        cur.close()
        self.ccams = [c[0] for c in ccams]
        """

    def generate_one(self, p):
        """
        - p patient
        """
        DP = rd.choice(self.cims)
        e = rd.choice(self.hospitals)
        stay = ShortHospStay(p, e, DP)

        stay.start_date = self.context.generate_date(begin=p.BD, end=date(2020, 1, 1))
        stay.finish_date = self.context.generate_date(
            begin=stay.start_date, end=date(2020, 1, 1)
        )

        # generate associated diagnosis (use aideaucodage.fr)
        for i in range(4):
            stay.cim_das.append(rd.choice(self.cims))  # associate/related diagnosis
        """
        stay.ccam = []
        """
        p.hospitalStays.append(stay)

    def generate(self, p, nbs=3):
        """
        - p patient
        - nbs number of hospitalisation to generate for a patient
        """
        for i in range(nbs):
            self.generate_one(p)

    def __load__(self, dd, p, colmap):
        cim = dd[colmap["cim"]]
        stay = ShortHospStay(p, rd.choice(self.hospitals), cim)
        """
        stay.start_date=self.context.generate_date(begin = p.BD, end=date(2020,1,1))
        stay.finish_date=self.context.generate_date(begin = p.BD, end=date(2020,1,1))
        """
        if "date" in colmap:
            stay.start_date = dd[colmap["date"]]
            stay.start_date = date(
                stay.start_date.year, stay.start_date.month, stay.start_date.day
            )
            if not "enddate" in colmap:
                stay.finish_date = stay.start_date
            else:
                stay.finish_date = dd[colmap["enddate"]]
                stay.finish_date = date(
                    stay.finish_date.year, stay.finish_date.month, stay.finish_date.day
                )

        return stay

    def load(self, p, df, colmap, drop_errors=False):
        """
        - df pandas dataframe containing a columwise description of an hospital stay.
            This data frame must at least contain an attribute with the patient
            id, an attribute with the CIM code, date and duration.

        - colmap a map that gives the column name in which the attributes of a
            patient have to be found.
            The mandatory attributes to define from columns are: 'cim'
            The possible attributes to define from columns are: 'date'
            The possible attributes to define from columns are: 'duration'

            In addition, the attribute 'idp' must define the column corresponding
            to the patient identifier (attribute containing the value of the index
            in the dataframe of patients, while patient was loaded and that is used
            as internal identifier, not necessarily the NIR)

        - drop_errors if True, the instances [TODO]

        The function ensures some validity checking:  [TODO]
            - some attributes are mandatory
            - the attributes are casted to the right type (otherwise errors are thrown)
            - some attribute constraints (such as string lengths)

        The function modifies the patient p instance
        """
        stays = df[df[colmap["pid"]] == p.__internal_id__].apply(
            self.__load__, p=p, colmap=colmap, axis=1
        )
        if len(stays) != 0:
            p.hospitalStays.extend(stays)

    def load_allpatients(self, patients, df, colmap, drop_errors=False):
        list(map(lambda p: self.load(p, df, colmap, drop_errors), patients))
