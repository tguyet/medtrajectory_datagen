#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module 

@author: Thomas Guyet
@date: 03/2023
"""

from .simulation import simulation
from .database_model import (
    Patient,
    ALD,
    GP,
    Specialist,
    Provider,
    DrugDelivery,
    Etablissement,
    MedicalVisit,
    MedicalAct,
    ShortHospStay,
)
from math import isnan
import numpy.random as rd
import pandas as pd
import numpy as np
import sqlite3  ##sqlite queries access nomenclatures
from itertools import chain

from typing import Union

from datetime import timedelta, date


class pathwayInjector:
    """A pathway injector is the procedural description of medical events to
    occur in a patient's history.

    The principle is to describe the sequence of events of a specific
    pathway (for example a specific treatment). This description can be
    probabilistic or deterministic: whatever you want! You inject directly
    the events.

    In addition, we require to define a function to identify the date index
    of the pathway, ie the date at which an occurrence of the pathway start in
    a patient history. It can start after a specific event (for instance an
    hospitalisation with a specific disease).

    Optionally, you can also add some preconditions on the patient to
    inject effectively the pathway.

    TODO handle multiple occurrences of the pathway when there are
    multiple date satisfying the injection constraints.
    """

    def __init__(self, sim: simulation):
        self.sim = sim

    def pathdesc(self, patient: Patient, d: date):
        """Description of the pathway

        This function must be overloaded to define the pathway to inject into
        the `patient` history at date `d`."""
        pass

    def injection(self, patient: Patient, d: date = pd.NaT):
        """Pathway injection function.
        The current pathway is injected to the patient `patient` in case
        it satisfies the precondition.

        The paramater `d` can be used to define the explicit date of the
        beginning of the pathway to inject in the patient history.
        If it is not provided then it is determined by the __index_date__
        function (if possible, otherwise nothing is injected)

        Parameters
        ============
        patient : Patient
            The patient to whom we attempt to inject the pathway
        """
        if not self.__precondition__(patient):
            return

        if d is pd.NaT:
            # try to find the injection date
            d = self.__index_date__(patient)
            if d is pd.NaT:
                # no possibilty
                return
        # Here: d gives the starting date to inject
        self.pathdesc(patient, d)

    def __precondition__(self, patient: Patient) -> bool:
        """Verification of the precondition required for a patient
        to hold this pathway.

        By default, there is no precondition.

        Parameters
        ==========
        p : Patient
            The patient to whom we attempt to inject the pathway

        Returns
        =======
        bool
            True if precondition are satisfied and False otherwise."""
        return True

    def __index_date__(self, patient: Patient, d: date = pd.NaT) -> date:
        """This function defines the date index for patient `p`.

        Parameters
        ==========
        p : Patient
            The patient to whom we attempt to inject the pathway
        d : date
            When defined (not pd.NaT)

        Returns
        ========
        date or pd.NaT
            Returns None if the function did no succeed to identify
            the date index for the pathway
        """
        return pd.NaT
