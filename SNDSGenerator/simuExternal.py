#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 21 14:51:40 2022

@author: tguyet
"""
import pandas as pd
from .simulation import simulation
import warnings

from .data_factory import FactoryContext, PatientFactory, \
    PhysicianFactory, PharmacyFactory, DrugsDeliveryFactory, \
        EtablissementFactory, VisitFactory, ActFactory, ShortStayFactory \



from .simu_open import OpenDataFactoryContext, OpenPhysicianFactory, OpenPharmacyFactory, \
    OpenEtablissementFactory



class simuExternal(simulation):
    def __init__(self, datarep, nomencl="snds_nomenclature.db"):
        super().__init__(nomencl=nomencl)
        #self.nomencl_db = nomencl
        #self.context = FactoryContext(nomenclatures=self.nomencl_db)
        #self.pharms = []
        #self.GPs = []
        #self.specialists = []
        #self.patients = []
        #self.etablissements = []
        
        self.__run__ = False
        self.datarep=datarep
        self.context = OpenDataFactoryContext(nomenclatures=nomencl,datarep=self.datarep)
        self.context.year=2019
        
        self.dpts=[22,35]
        self.nb_patients=100 #0 means all
        self.nb_physicians=100 #0 means all
        
        self.verbose=False

    def run(self):
        ## Liberal physicians
        factory = OpenPhysicianFactory(self.context, self.dpts)
        physicians= factory.generate(self.nb_physicians)
        
        #drugs facet
        factory = OpenPharmacyFactory(self.context, self.dpts)
        self.pharms= factory.generate(100)
        
        #Generate one Hospital
        self.etablissements.append( EtablissementFactory(self.context).generate() )

        #General practicioners
        self.GPs =  [ p for p in physicians if p.speciality==1 ]
        self.specialists =  [ p for p in physicians if p.speciality!=1 ]
        
        #hospitals
        factory = OpenEtablissementFactory(self.context, self.dpts)
        self.etablissements= factory.generate(30)

        self.__run__=True
        
        
    def load(self, simu):
        """
        Load a simulation description

        simu {'patients': {'data':pd.dataframe, 'attributes':{}}, 'drugs': {'data':pd.dataframe, 'attributes':{}}, 'ALD': pd.dataframe}
        """
        if not self.__run__:
            warnings.warn("Run the model before loading")
            return

        #generate the patients 
        factory = PatientFactory(self.context, self.GPs)
        self.patients=factory.load(simu['patients']['data'], simu['patients']['attributes'])

        if "drugs" in simu:
            #generate the drug deliveries        
            drugfact=DrugsDeliveryFactory(self.context, self.pharms)
            drugfact.load_allpatients(self.patients, simu['drugs']['data'], simu['drugs']['attributes'])
        
        if "hosps" in simu:
            #generate the short stays
            hospital=ShortStayFactory(self.context, self.etablissements)
            hospital.load_allpatients(self.patients, simu['hosps']['data'], simu['hosps']['attributes'])
    
            
if __name__ == "__main__":
    import numpy as np

    ##Generate a population
    pop=pd.read_csv("../datarep/pop.csv").drop(['Unnamed: 0'],axis=1)
    pop=pop.dropna()
    pop['pop']/=np.sum(pop['pop'])

    population=pop.iloc[np.random.choice(len(pop['pop']),p=pop['pop'],size=1000)].drop(['pop'],axis=1).reset_index(drop=True)

    #Generate some random drug deliveries
    drugs_freq=pd.read_csv("../datarep/drugs_freq.csv").drop(['Unnamed: 0'],axis=1)
    nb_deliveries = pd.read_csv("../datarep/mean_deliveries.csv").drop(['Unnamed: 0'],axis=1)

    # generation to do
    to_generate=population[['age','sex','RR']].groupby(['age','sex','RR']).size().reset_index().rename(columns={0:"nb"})
    to_generate=pd.merge(to_generate, nb_deliveries, on=['age','sex','RR'])
    to_generate['nb']=np.round(to_generate['nb']*to_generate['mean'])
    to_generate.drop('mean', axis=1, inplace=True)
    to_generate=to_generate[to_generate['nb']!=0]

    #for a given set of characteristics to generate drugs
    def generate_drugs(x):
        age=x['age']
        sex=x['sex']
        RR=x['RR']
        nb=int(x['nb'])

        possible_drugs=drugs_freq[(drugs_freq['age']==age) & (drugs_freq['sex']==sex) & (drugs_freq['RR']==RR)][['CIP13','p']]
        if len(possible_drugs)==0:
            return None
        possible_drugs['p']/=np.sum(possible_drugs['p'])

        loc_drugs=possible_drugs.iloc[np.random.choice(len(possible_drugs),p=possible_drugs['p'],size=nb)].drop(['p'],axis=1).reset_index(drop=True)

        #list of the people having the characteristics
        indices=population[(population['age']==age) & (population['sex']==sex) & (population['RR']==RR)].reset_index()['index']
        loc_drugs['patient']=np.random.choice(indices, size=len(loc_drugs))
        return loc_drugs

    res=to_generate.apply(generate_drugs, axis=1)
    drugs=pd.concat(res.to_list())

    hosps = pd.DataFrame({'pid':[], 'CIP':[], 'start':[]})

    # representation of a simulation with datasets
    simu = {
            "patients":{
                    "data":population,
                    "attributes":{'sex':'sex', 'age':'age'}
                    },
            "drugs":{
                    "data":drugs,
                    "attributes":{'pid':'patient','cip':'CIP13'}
                    },
            "hospstays":{
                    "data":hosps,
                    "attributes":{'pid':'patient', 'cim':'ICD', 'startdate':'start'}
                    }
    }


    sim = simuExternal(nomencl="../datarep/snds_nomenclature.db",
                     datarep="../datarep")
    sim.run()
    sim.load(simu)

    for p in sim.patients:
        for dd in p.drugdeliveries:
            print(dd)
        for dd in p.visits:
            print(dd)
        for dd in p.medicalacts:
            print(dd)
        for dd in p.hospitalStays:
            print(dd)
        print("========")
