"""Script for simulating a database inspired by the VICAN study (breast cancer)

The statistical modeling for this study has been conducted by M. Guyomard (SESSTIM) 
and R. Urena (SESSTIM).
"""

import logging
import random as rd
import numpy as np
import pandas as pd
from scipy.stats import truncnorm, bernoulli

from pathways import P1, P2, P3, P4, P5, P6, P7, P8, P9, P10
from SNDSGenerator.simuExternal import simuExternal
from SNDSGenerator.simulationDB import simDB

# pylint: disable=C0103,C0116


activate_debug=True
if activate_debug:
    level = logging.DEBUG
else:
    level = logging.INFO

logging.basicConfig(filename="debug.log", encoding="utf-8", level=level)

logging.debug("====  Generate VICAN Database ====")

# Nombre total de points à générer
# total_points = 10500
total_patients = 10500

logging.debug(f"Create population of {total_patients} patients ...")

# Définition des proportions pour chaque tranche d'âge
proportions = [0.2143, 0.2571, 0.2857, 0.2429]

# Ajuster les paramètres pour les lois normales tronquées
# (moyenne, écart-type, borne inférieure, borne supérieure)
params = [
    (48, 7, 20, 49),  # <50 ans
    (54, 3.5, 50, 59),  # 50-59 ans
    (65, 4, 60, 69),  # 60-69 ans
    (75, 6, 70, 95),  # ≥70 ans
]

# Calcul des effectifs en arrondissant à l'entier le plus proche
n_points_per_group = [int(round(total_patients * prop)) for prop in proportions]

# Ajuster le dernier groupe pour compenser l'écart
n_points_per_group[-1] += total_patients - sum(n_points_per_group)


# Fonction pour générer les données en utilisant une loi normale tronquée
def generate_truncated_normal(mean, std, lower, upper, n):
    a, b = (lower - mean) / std, (upper - mean) / std
    return truncnorm.rvs(a, b, loc=mean, scale=std, size=n)


# Génération des données
samples1 = []
age_labels = []
for i, (n_points, (mean, std, lower, upper)) in enumerate(
    zip(n_points_per_group, params)
):
    samples1.extend(generate_truncated_normal(mean, std, lower, upper, n_points))
    age_labels.extend([i] * n_points)

# Convertir en numpy array pour manipulation
samples1 = np.array(np.round(samples1))

# creation of the population table representing the population :
# - all patients have an Undefined status in this profile
population = pd.DataFrame({"sex": 1, "age": samples1, "status": "U"})


def status_nodal(p):
    if p.age < 50:
        return 1 - bernoulli.rvs(0.87)
    elif p.age <= 59:
        return 1 - bernoulli.rvs(0.94)
    elif p.age <= 69:
        return 1 - bernoulli.rvs(0.94)
    else:
        return 1 - bernoulli.rvs(0.94)


population["nodal_status"] = population.apply(status_nodal, axis=1)


if activate_debug:
    ax= population.plot.hist(column=["age"], bins=20)
    fig = ax.get_figure()
    fig.savefig('ages.pdf')

def assign_pathway(p):
    if p.age < 50:
        p = {
            "P1": 1.5,
            "P2": 0.7,
            "P3": 2.6,
            "P4": 5.1,
            "P5": 2,
            "P6": 0.7,
            "P7": 0.7,
            "P8": 2,
            "P9": 2.8,
            "P10": 5.5,
        }
    elif p.age <= 59:
        p = {
            "P1": 1.8,
            "P2": 1,
            "P3": 3.1,
            "P4": 7.9,
            "P5": 0.7,
            "P6": 0.5,
            "P7": 0.6,
            "P8": 1.8,
            "P9": 2,
            "P10": 5.2,
        }
    elif p.age <= 69:
        p = {
            "P1": 2,
            "P2": 1.4,
            "P3": 3.3,
            "P4": 10.7,
            "P5": 0.3,
            "P6": 0.5,
            "P7": 0.4,
            "P8": 1.4,
            "P9": 1.8,
            "P10": 5.4,
        }
    else:
        p = {
            "P1": 1.7,
            "P2": 1.9,
            "P3": 3,
            "P4": 13.3,
            "P5": 0,
            "P6": 0.3,
            "P7": 0.3,
            "P8": 0.8,
            "P9": 0.9,
            "P10": 2.4,
        }
    return rd.choices(list(p.keys()), weights=p.values())[0]


population["pathway"] = population.apply(assign_pathway, axis=1)

if activate_debug:
    ax= population.groupby("pathway").size().plot.bar()
    fig = ax.get_figure()
    fig.savefig('pathways.pdf')

logging.debug("Start simulation ...")

simu = {
    "patients": {
        "data": population,
        "attributes": {
            "sex": "sex",
            "age": "age",
        },
    }
}

simulator = simuExternal(nomencl="datarep/snds_nomenclature.db", datarep="datarep")
simulator.run()
simulator.load(simu)

simulator.context.year = 2022

# injection of the pathways
for P in [P1, P2, P3, P4, P5, P6, P7, P8, P9, P10]:
    p = P(simulator)
    for patient in simulator.patients:
        p.injection(patient)


logging.debug("Start DB generation ...")

dbgen = simDB()
dbgen.output_db_name = "Models/VICAN/snds_testgen.db"
dbgen.generate(simulator, rootschemas="schema-snds/schemas")
