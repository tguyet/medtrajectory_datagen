
##### Simulation âge

import logging

import numpy as np
import pandas as pd
from scipy.stats import truncnorm

from pathways import P1, P2, P3, P4, P5, P6, P7, P8, P9, P10
from SNDSGenerator.simuExternal import simuExternal
from SNDSGenerator.simulationDB import simDB


logging.basicConfig(filename="debug.log", encoding="utf-8", level=logging.DEBUG)

total_patients = 100

# Définition des proportions pour chaque tranche d'âge
proportions = [0.2143, 0.2571, 0.2857, 0.2429]

# Ajuster les paramètres pour les lois normales tronquées
# (moyenne, écart-type, borne inférieure, borne supérieure)
params = [
    (48, 7, 20, 49),  # <50 ans
    (54, 3.5, 50, 59),  # 50-59 ans
    (65, 4, 60, 69),  # 60-69 ans
    (75, 6, 70, 95),  # ≥70 ans
]

# Calcul des effectifs en arrondissant à l'entier le plus proche
n_points_per_group = [int(round(total_patients * prop)) for prop in proportions]

# Ajuster le dernier groupe pour compenser l'écart
n_points_per_group[-1] += total_patients - sum(n_points_per_group)


# Fonction pour générer les données en utilisant une loi normale tronquée
def generate_truncated_normal(mean, std, lower, upper, n):
    a, b = (lower - mean) / std, (upper - mean) / std
    return truncnorm.rvs(a, b, loc=mean, scale=std, size=n)


# Génération des données
samples1 = []
age_labels = []
for i, (n_points, (mean, std, lower, upper)) in enumerate(
    zip(n_points_per_group, params)
):
    samples1.extend(generate_truncated_normal(mean, std, lower, upper, n_points))
    age_labels.extend([i] * n_points)

# Convertir en numpy array pour manipulation
samples1 = np.array(np.round(samples1))

# creation of the population table representing the population :
# - all patients have an Undefined status in this profile
population = pd.DataFrame({"sex": 1, "age": samples1, "status": "U"})

population["pathway"] = "P6"

simu = {
    "patients": {
        "data": population,
        "attributes": {
            "sex": "sex",
            "age": "age",
        },
    }
}

simulator = simuExternal(nomencl="datarep/snds_nomenclature.db", datarep="datarep")
simulator.run()
simulator.load(simu)

simulator.context.year = 2022

# injection of the pathways
for P in [P1, P2, P3, P4, P5, P6, P7, P8, P9, P10]:
    p = P(simulator)
    for patient in simulator.patients:
        p.injection(patient)

dbgen = simDB()
dbgen.output_db_name = "Models/VICAN/snds_vican.db"
dbgen.generate(simulator, rootschemas="schema-snds/schemas")
