"""
Description of the VICAN pathways
"""

import warnings
from datetime import date
from dateutil.relativedelta import relativedelta

import numpy as np
import numpy.random as rd
import pandas as pd


from SNDSGenerator.pathway_factory import pathwayInjector
from SNDSGenerator.database_model import (
    Patient,
    ShortHospStay,
    DrugDelivery,
    MedicalAct,
)
from SNDSGenerator.simulation import simulation

# pylint: disable=C0103


class Surgery(pathwayInjector):
    """Surgery pathway

    This pathway includes only one specific surgery (short hospital stay).
    """

    def __init__(self, sim: simulation):
        super().__init__(sim)
        # CCAM codes for breast cancer surgeries
        self.code_chir = [
            "QEFA001",
            "QEFA008",
            "QEFA004",
            "QEFA016",
            "QEFA017",
            "QEFA018",
            "QEFA003",
            "QEF005",
            "QEFA010",
            "QEFA020",
            "QEFA007",
            "QEFA012",
            "QEFA013",
            "QEFA015",
            "QEFA019",
        ]

    def pathdesc(self, patient: Patient, d: date):
        hospit_reason = "C50"  # C50.9	Tumeur maligne du sein, sans précision
        surgery_code = rd.choice(self.code_chir)

        care = ShortHospStay(patient, self.sim.etablissements[0], hospit_reason)
        care.ccam.append(surgery_code)
        care.start_date = d
        care.finish_date = d

        if patient.raw_data['nodal_status']==1:
            care.cim_das.append("C773")

        patient.append(care)
        return d


class ET_uni(pathwayInjector):
    """Endocrino Therapy pathway"""

    def __init__(self, sim: simulation):
        super().__init__(sim)
        code_ET_ATC_AI = ["L02BA01"]
        code_ET_ATC_Tamoxifen = ["L02BG03", "L02BG06", "L02BG04"]

        # SELECT PHA_CIP_C13 from IR_PHA_r where PHA_ATC_C07 like "L02BA01" or  PHA_ATC_C07 like "L02BG03" or  PHA_ATC_C07 like "L02BG06" or  PHA_ATC_C07 like "L02BG04" or  PHA_ATC_C07 like "L02AE03" or  PHA_ATC_C07 like "L02AE02" or  PHA_ATC_C07 like "L02AE04";
        self.code_ET_CIP_L02BA01 = [
            "3400932059324",
            "3400932865284",
            "3400932903467",
            "3400932903696",
            "3400932921508",
            "3400932921737",
            "3400933179441",
            "3400933263867",
            "3400933263928",
            "3400933288303",
            "3400933430542",
            "3400933952235",
            "3400933952464",
            "3400934106323",
            "3400934106491",
            "3400934303623",
            "3400934303791",
            "3400934317811",
            "3400934639302",
            "3400934639470",
            "3400934639531",
            "3400935646316",
            "3400935646484",
            "3400935795908",
            "3400935825407",
            "3400935833440",
            "3400935940230",
            "3400935940469",
            "3400936136540",
            "3400936136601",
            "3400936143463",
            "3400936143524",
            "3400936143692",
            "3400936476189",
            "3400936476240",
            "3400936663954",
            "3400936664036",
            "3400936797598",
            "3400936797659",
        ]
        self.code_ET_CIP_Tamoxifen = [
            "3400921636116",
            "3400921654783",
            "3400921792102",
            "3400921997507",
            "3400922133263",
            "3400922133324",
            "3400922136165",
            "3400922136455",
            "3400922211428",
            "3400930108338",
            "3400930108345",
            "3400930117248",
            "3400930162309",
            "3400934147425",
            "3400934236464",
            "3400934236525",
            "3400934625923",
            "3400934626234",
            "3400934712081",
            "3400935279668",
            "3400935901149",
            "3400935901897",
            "3400935985354",
            "3400935985415",
            "3400936037625",
            "3400938276749",
            "3400938294088",
            "3400938434378",
            "3400938434439",
            "3400938435030",
            "3400938435610",
            "3400938436150",
            "3400938436679",
            "3400938515084",
            "3400938515145",
            "3400938515664",
            "3400938943054",
            "3400939095219",
            "3400939161365",
            "3400939259802",
            "3400939331751",
            "3400939413709",
            "3400939413877",
            "3400939439372",
            "3400939664538",
            "3400939665078",
            "3400939665368",
            "3400939665429",
            "3400939764634",
            "3400939851952",
            "3400939986609",
            "3400939986838",
            "3400939987378",
            "3400939987668",
            "3400939987958",
            "3400939988030",
            "3400939988320",
            "3400941509377",
            "3400941519314",
            "3400941520204",
            "3400941553899",
            "3400941554490",
            "3400941557392",
            "3400941627682",
            "3400941677779",
            "3400941950124",
            "3400941950414",
            "3400949000029",
            "3400949000401",
            "3400949000487",
            "3400949000647",
            "3400949000784",
            "3400949000937",
            "3400949000944",
            "3400949001859",
            "3400949001958",
            "3400949001972",
            "3400949002139",
            "3400949003150",
            "3400949004911",
            "3400949008292",
            "3400949011483",
            "3400949013845",
            "3400949015627",
            "3400949016167",
            "3400949017348",
            "3400949017577",
            "3400949019939",
            "3400949020188",
            "3400949027743",
            "3400949140855",
            "3400949185801",
            "3400949188123",
            "3400949190133",
            "3400949205592",
            "3400949248537",
            "3400949252619",
            "3400949275670",
            "3400949275731",
            "3400949276332",
            "3400949277162",
            "3400949277513",
            "3400949277681",
            "3400949295371",
            "3400949410019",
            "3400949410248",
            "3400949410477",
            "3400949452460",
            "3400949497294",
            "3400949497706",
            "3400949626359",
            "3400949630950",
            "3400949640836",
            "3400949855827",
            "3400949881789",
            "3400949882151",
            "3400949916542",
            "3400949931859",
            "3400949980994",
            "3400949982028",
        ]

    def pathdesc(self, patient: Patient, d: date):
        # tirage d'un sur les deux
        if rd.choice([0, 1], p=[0.255, 0.745]):
            ET_code = rd.choice(self.code_ET_CIP_L02BA01)
        else:
            ET_code = rd.choice(self.code_ET_CIP_Tamoxifen)
        nb = 12
        for i in range(nb):  # combien de temps ??
            care = DrugDelivery(ET_code, patient, self.sim.pharms[0])
            care.date_debut = d
            care.date_fin = d
            patient.append(care)
            if i != nb - 1:
                d = d + relativedelta(months=1)
        return d


class ET_bi(pathwayInjector):
    """Endocrino Therapy"""

    def __init__(self, sim: simulation):
        super().__init__(sim)
        code_ET_ATC_AI = ["L02BG03", "L02BG06", "L02BG04"]
        code_ET_ATC_Tamoxifen = ["L02BA01"]

        # SELECT PHA_CIP_C13 from IR_PHA_r where PHA_ATC_C07 like "L02BA01" or  PHA_ATC_C07
        # like "L02BG03" or  PHA_ATC_C07 like "L02BG06" or  PHA_ATC_C07 like "L02BG04" or
        # PHA_ATC_C07 like "L02AE03" or  PHA_ATC_C07 like "L02AE02" or
        # PHA_ATC_C07 like "L02AE04";
        self.code_ET_CIP_Tamoxifen = [
            "3400932059324",
            "3400932865284",
            "3400932903467",
            "3400932903696",
            "3400932921508",
            "3400932921737",
            "3400933179441",
            "3400933263867",
            "3400933263928",
            "3400933288303",
            "3400933430542",
            "3400933952235",
            "3400933952464",
            "3400934106323",
            "3400934106491",
            "3400934303623",
            "3400934303791",
            "3400934317811",
            "3400934639302",
            "3400934639470",
            "3400934639531",
            "3400935646316",
            "3400935646484",
            "3400935795908",
            "3400935825407",
            "3400935833440",
            "3400935940230",
            "3400935940469",
            "3400936136540",
            "3400936136601",
            "3400936143463",
            "3400936143524",
            "3400936143692",
            "3400936476189",
            "3400936476240",
            "3400936663954",
            "3400936664036",
            "3400936797598",
            "3400936797659",
        ]
        self.code_ET_CIP_AI = [
            "3400921636116",
            "3400921654783",
            "3400921792102",
            "3400921997507",
            "3400922133263",
            "3400922133324",
            "3400922136165",
            "3400922136455",
            "3400922211428",
            "3400930108338",
            "3400930108345",
            "3400930117248",
            "3400930162309",
            "3400934147425",
            "3400934236464",
            "3400934236525",
            "3400934625923",
            "3400934626234",
            "3400934712081",
            "3400935279668",
            "3400935901149",
            "3400935901897",
            "3400935985354",
            "3400935985415",
            "3400936037625",
            "3400938276749",
            "3400938294088",
            "3400938434378",
            "3400938434439",
            "3400938435030",
            "3400938435610",
            "3400938436150",
            "3400938436679",
            "3400938515084",
            "3400938515145",
            "3400938515664",
            "3400938943054",
            "3400939095219",
            "3400939161365",
            "3400939259802",
            "3400939331751",
            "3400939413709",
            "3400939413877",
            "3400939439372",
            "3400939664538",
            "3400939665078",
            "3400939665368",
            "3400939665429",
            "3400939764634",
            "3400939851952",
            "3400939986609",
            "3400939986838",
            "3400939987378",
            "3400939987668",
            "3400939987958",
            "3400939988030",
            "3400939988320",
            "3400941509377",
            "3400941519314",
            "3400941520204",
            "3400941553899",
            "3400941554490",
            "3400941557392",
            "3400941627682",
            "3400941677779",
            "3400941950124",
            "3400941950414",
            "3400949000029",
            "3400949000401",
            "3400949000487",
            "3400949000647",
            "3400949000784",
            "3400949000937",
            "3400949000944",
            "3400949001859",
            "3400949001958",
            "3400949001972",
            "3400949002139",
            "3400949003150",
            "3400949004911",
            "3400949008292",
            "3400949011483",
            "3400949013845",
            "3400949015627",
            "3400949016167",
            "3400949017348",
            "3400949017577",
            "3400949019939",
            "3400949020188",
            "3400949027743",
            "3400949140855",
            "3400949185801",
            "3400949188123",
            "3400949190133",
            "3400949205592",
            "3400949248537",
            "3400949252619",
            "3400949275670",
            "3400949275731",
            "3400949276332",
            "3400949277162",
            "3400949277513",
            "3400949277681",
            "3400949295371",
            "3400949410019",
            "3400949410248",
            "3400949410477",
            "3400949452460",
            "3400949497294",
            "3400949497706",
            "3400949626359",
            "3400949630950",
            "3400949640836",
            "3400949855827",
            "3400949881789",
            "3400949882151",
            "3400949916542",
            "3400949931859",
            "3400949980994",
            "3400949982028",
        ]
        self.code_ET_CIP_agonist = [
            "3400930020005",
            "3400930122112",
            "3400930122129",
            "3400932829217",
            "3400932850037",
            "3400932850266",
            "3400932932672",
            "3400933129125",
            "3400933943769",
            "3400933963811",
            "3400934125423",
            "3400934125652",
            "3400935844675",
            "3400936690868",
            "3400936690929",
            "3400937575256",
            "3400937575317",
            "3400938081459",
            "3400938263381",
            "3400938458381",
            "3400939262123",
            "3400939890173",
            "3400949002634",
            "3400949002818",
        ]

    def pathdesc(self, patient: Patient, d: date):

        nb_tot = 12

        seq_type = rd.choice([1, 2, 3, 4], p=[0.5167, 0.4, 0.05, 0.0333])
        if seq_type == 1:
            ET_code_AI = rd.choice(self.code_ET_CIP_AI)
            ET_code_Tamoxifen = rd.choice(self.code_ET_CIP_Tamoxifen)
            nb = rd.randint(1, nb_tot - 1)
            for i in range(nb):
                care = DrugDelivery(ET_code_Tamoxifen, patient, self.sim.pharms[0])
                care.date_debut = d
                care.date_fin = d
                patient.append(care)
            for i in range(nb_tot - nb):
                care = DrugDelivery(ET_code_AI, patient, self.sim.pharms[0])
                care.date_debut = d
                care.date_fin = d
                patient.append(care)
                if i != nb - 1:
                    d = d + relativedelta(months=1)

        elif seq_type == 2:

            ET_code_AI = rd.choice(self.code_ET_CIP_AI)
            ET_code_Tamoxifen = rd.choice(self.code_ET_CIP_Tamoxifen)
            nb = rd.randint(1, nb_tot - 1)
            for i in range(nb):
                care = DrugDelivery(ET_code_AI, patient, self.sim.pharms[0])
                care.date_debut = d
                care.date_fin = d
                patient.append(care)
            for i in range(nb_tot - nb):
                care = DrugDelivery(ET_code_Tamoxifen, patient, self.sim.pharms[0])
                care.date_debut = d
                care.date_fin = d
                patient.append(care)
                if i != nb - 1:
                    d = d + relativedelta(months=1)

        elif seq_type == 3:
            ET_code_Tamoxifen = rd.choice(self.code_ET_CIP_Tamoxifen)
            ET_code_agonist = rd.choice(self.code_ET_CIP_agonist)
            for i in range(nb_tot):  # combien de temps ??
                care = DrugDelivery(ET_code_Tamoxifen, patient, self.sim.pharms[0])
                care.date_debut = d
                care.date_fin = d
                patient.append(care)
                care = DrugDelivery(ET_code_agonist, patient, self.sim.pharms[0])
                care.date_debut = d
                care.date_fin = d
                patient.append(care)
                if i != nb_tot - 1:
                    d = d + relativedelta(months=1)

        elif seq_type == 4:
            ET_code_AI = rd.choice(self.code_ET_CIP_AI)
            ET_code_agonist = rd.choice(self.code_ET_CIP_agonist)
            for i in range(nb_tot):  # combien de temps ??
                care = DrugDelivery(ET_code_agonist, patient, self.sim.pharms[0])
                care.date_debut = d
                care.date_fin = d
                patient.append(care)
                care = DrugDelivery(ET_code_AI, patient, self.sim.pharms[0])
                care.date_debut = d
                care.date_fin = d
                patient.append(care)
                if i != nb_tot - 1:
                    d = d + relativedelta(months=1)
        return d


class RT(pathwayInjector):
    """Radiotherapy pathway (abstract class)"""

    def __init__(self, sim: simulation):
        super().__init__(sim)

        v = rd.binomial(1, 0.5)
        if v == 1:
            self.concrete_injector = RT_outHospital(sim)
        else:
            self.concrete_injector = RT_inHospital(sim)

    def pathdesc(self, patient: Patient, d: date):
        # warnings.warn("RT is an abstract pathway that to not inject anything !!")
        return self.concrete_injector.pathdesc(patient, d)


class RT_outHospital(pathwayInjector):
    """Radiotherapy pathway when delivered **out** a public hospital

    The RT is organised as a single stay with multi sessions (RUM).

    NB: always finish by Monday + Thursday sequences
    do not guaranty exact 12 weeks
    """

    def __init__(self, sim: simulation):
        super().__init__(sim)

        codes = {
            "YYYY166": 5326,
            "YYYY047": 4280,
            "YYYY152": 3758,
            "YYYY128": 2863,
            "YYYY048": 2299,
            "YYYY323": 2006,
            "YYYY334": 1562,
            "YYYY050": 837,
            "YYYY049": 829,
            "YYYY109": 779,
            "YYYY244": 756,
            "YYYY197": 732,
            "YYYY387": 412,
            "YYYY356": 354,
            "YYYY347": 353,
            "YYYY345": 314,
            "YYYY469": 308,
            "YYYY081": 298,
            "YYYY080": 246,
            "YYYY380": 246,
            "YYYY307": 228,
            "YYYY337": 218,
            "YYYY151": 212,
            "YYYY304": 205,
            "YYYY458": 186,
            "YYYY460": 185,
            "YYYY369": 168,
            "YYYY331": 154,
            "YYYY225": 148,
            "YYYY370": 138,
            "YYYY377": 133,
            "YYYY491": 129,
            "YYYY398": 122,
            "YYYY391": 115,
            "YYYY299": 111,
            "YYYY459": 108,
            "YYYY358": 104,
        }

        # draw the CCAM code used for this patient all along the RT
        p = np.array(list(codes.values()), dtype="float64")
        p = p / sum(p)
        self.act_code = rd.choice(list(codes), p=p)

    def pathdesc(self, patient: Patient, d: date):
        # create the RT session
        for _ in range(24):
            care = MedicalAct(self.act_code, patient, self.sim.etablissements[0])
            care.date_debut = d
            care.date_fin = d
            patient.append(care)
            d = d + relativedelta(days=3)
            if date.weekday(d) == 5:  # Saturday
                d += relativedelta(days=1)
            elif date.weekday(d) == 6:  # Sunday
                d += relativedelta(days=2)
        return d


class RT_inHospital(pathwayInjector):
    """Radiotherapy pathway when delivered in a public hospital

    The RT is organised as one first visit and then a collection of sessions.
    Each session has its own RSS (MCO stay)


    """

    def __init__(self, sim: simulation):
        super().__init__(sim)

        codes = {
            "ZZNL065": 34192,
            "ZZNL063": 14296,
            "YYYY047": 11402,
            "ZZNL062": 5858,
            "ZZNL064": 5207,
            "YYYY152": 4953,
            "YYYY166": 4639,
            "ZZNL053": 3001,
            "YYYY307": 2318,
            "YYYY048": 1992,
            "ZZNL050": 1739,
            "ZZNL037": 1507,
            "YYYY049": 1301,
            "ZZNL040": 1266,
            "YYYY450": 1144,
            "YYYY128": 1113,
            "YYYY347": 1012,
            "ZZNL031": 936,
            "ZZNL048": 833,
            "ZZNL054": 791,
            "ZZNL039": 753,
            "ZZNL043": 675,
            "YYYY459": 552,
            "YYYY197": 528,
            "YYYY387": 488,
            "YYYY050": 479,
            "ZZNL036": 478,
            "ZZNL051": 456,
            "ZZNL061": 363,
            "YYYY046": 343,
            "ZZNL030": 325,
            "YYYY304": 290,
            "YYYY337": 229,
            "YYYY081": 223,
            "YYYY492": 211,
            "YYYY109": 166,
            "YYYY151": 164,
            "ZZNL042": 156,
            "YYYY080": 118,
            "YYYY136": 114,
        }

        # draw the CCAM code used for this patient all along the RT
        p = np.array(list(codes.values()), dtype="float64")
        p = p / sum(p)
        self.act_code = rd.choice(list(codes), p=p)

    def pathdesc(self, patient: Patient, d: date):
        # create the stay initiating the Radio therapy
        care = ShortHospStay(patient, self.sim.etablissements[0], "Z5100")
        care.DRel = "C50"

        if date.weekday(d) == 5:  # Saturday
            d += relativedelta(days=2)
        elif date.weekday(d) == 6:  # Sunday
            d += relativedelta(days=1)
        care.start_date = d
        care.finish_date = d

        # add a RUM
        rum = ShortHospStay.hospRUM(42, "Z5101")
        rum.length = 1
        rum.DRel = "C50"
        care.RUMs.append(rum)
        patient.append(care)

        # next event
        if date.weekday(d) == 4:  # d was a Friday
            d += relativedelta(days=3)
        else:
            d += relativedelta(days=1)

        nb = rd.randint(5, 8)  # 5 to 7 week of treatment

        # create one MCO (RSS) per RT session (Z5101: irradiation session)
        #   The first session in the same day
        for _ in range(nb * 5 - 1):
            # session everyday, except the weekend
            care = ShortHospStay(patient, self.sim.etablissements[0], "Z5101")
            care.DRel = "C50"
            care.start_date = d
            care.finish_date = d
            care.ccam.append(self.act_code)
            care.RUMs.append(
                ShortHospStay.hospRUM(42, "Z5101")
            )  # add the corresponding RUM in a ambulatory service
            patient.append(care)

            if date.weekday(d) == 6:  # d was a Sunday, might never happen!!
                d += relativedelta(days=1)
            elif date.weekday(d) == 5:  # d was a Saturday, might never happen!!
                d += relativedelta(days=2)
            elif date.weekday(d) == 4:  # d was a Friday
                d += relativedelta(days=3)
            else:
                d += relativedelta(days=1)
        return d


class CT_uni(pathwayInjector):
    """Chimiotherapy pathway, unitherapy

    Notes
    -------
    * the starting date may be modified to be sure to be on a working day.
    """

    def __init__(self, sim: simulation, nb: int = None, delay: int = 7):
        super().__init__(sim)
        self.code_CT_CIM10 = ["Z511", "Z512"]
        self.code_CT_CCAM = {
            "ZZLF900": 826,
            "EBLA001": 19,
            "ABLB006": 0,
            "AFLB003": 77,
            "AFLB013": 76,
            "ECLF005": 0,
            "ECLF006": 0,
            "EELF004": 0,
            "EELF005": 0,
            "EDLF014": 0,
            "EDLF015": 0,
            "EDLF016": 5,
            "EDLF017": 0,
            "EDLF018": 0,
            "EDLF019": 0,
            "EDLF020": 0,
            "EDLF021": 0,
            "EBLF002": 0,
            "EBLF003": 0,
            "GGLB001": 0,
            "GGLB008": 0,
            "HPLB002": 0,
            "HPLB003": 4,
            "HPLB007": 0,
            "ZZLF004": 10,
        }

        # UCD codes: obtained from the requests: select PHA_ATC_C07,PHA_CIP_UCD from IR_PHA_R where PHA_ATC_C07 in ("L01DB03","L01AA01", "L01BC02", "L01CD01")
        # complemented with UCD codes deduced from the NU_UCD_R table (Reference table for UCD) using the drug name (ATC code)
        # L01AA01: CYCLOPHOSPHAMIDE -> ??
        # L01CD02: TAXOTERE -> "9178082","9178099" // plus utilisé depuis 2012
        # L01DB03: EPIRUBICINE -> "9284306","9284312","9284329","9284335","9284341","9284358","9284364","9284370","9284387","9284393","9284401","9284418","9284424","9285257","9285263","9285286","9285292","9285300","9285317","9285323","9285346","9285352","9293357","9293363","9293386","9293392","9304457","9304463","9304486","9304492","9305095","9305103","9305126","9305132","9306605","9306611","9306628","9306634","9306640","9306657","9306663","9306686","9313137","9313143","9313166","9313172","9313189","9316058","9316064","9316070","9319708","9319714","9328481","9332637","9332643","9337913","9337936","9337942","9337959","9343724","9343730","9343747","9343753"
        # L01BC02: Fluorouracile -> "9191527","9191533","9191556","9192107","9192113","9192136","9196163","9196186","9196192","9196200","9216925","9227685","9227691","9227716","9227722","9247191","9247216","9247222","9247239","9257315","9268276","9268282","9334866","9334872","9334889","9334895","9340950","9340967","9340973","9340996","9341004"
        # L01CD01: paclitaxel -> "9278091","9278116","9278122","9282744","9282750","9282767","9282773","9284499","9284507","9284513","9284536","9285381","9285398","9285406","9285412","9290459","9290465","9290471","9296829","9296835","9296841","9296858","9299443","9299466","9299472","9299489","9305379","9305385","9305391","9308254","9308923","9308946","9308952","9308969","9319022","9327903","9327926","9327932","9327949","9332809","9332815","9332821","9363282","9363299","9363307","9366441","9366458","9401350","9401367",

        self.code_CT_ATC = {
            "L01AA01": ["9031083", "9031077", "9031114"],
            #"L01CD02": ["9178082", "9178099"],
            "L01DB03": [
                "9145332",
                "9145361",
                "9284306",
                "9284312",
                "9284329",
                "9284335",
                "9284341",
                "9284358",
                "9284364",
                "9284370",
                "9284387",
                "9284393",
                "9284401",
                "9284418",
                "9284424",
                "9285257",
                "9285263",
                "9285286",
                "9285292",
                "9285300",
                "9285317",
                "9285323",
                "9285346",
                "9285352",
                "9293357",
                "9293363",
                "9293386",
                "9293392",
                "9304457",
                "9304463",
                "9304486",
                "9304492",
                "9305095",
                "9305103",
                "9305126",
                "9305132",
                "9306605",
                "9306611",
                "9306628",
                "9306634",
                "9306640",
                "9306657",
                "9306663",
                "9306686",
                "9313137",
                "9313143",
                "9313166",
                "9313172",
                "9313189",
                "9316058",
                "9316064",
                "9316070",
                "9319708",
                "9319714",
                "9328481",
                "9332637",
                "9332643",
                "9337913",
                "9337936",
                "9337942",
                "9337959",
                "9343724",
                "9343730",
                "9343747",
                "9343753",
            ],
            "L01BC02": [
                "9224505",
                "9030534",
                "9224505",
                "9303995",
                "9191527",
                "9191533",
                "9191556",
                "9192107",
                "9192113",
                "9192136",
                "9196163",
                "9196186",
                "9196192",
                "9196200",
                "9216925",
                "9227685",
                "9227691",
                "9227716",
                "9227722",
                "9247191",
                "9247216",
                "9247222",
                "9247239",
                "9257315",
                "9268276",
                "9268282",
                "9334866",
                "9334872",
                "9334889",
                "9334895",
                "9340950",
                "9340967",
                "9340973",
                "9340996",
                "9341004",
            ],
            "L01CD01": [
                "9278091",
                "9278116",
                "9278122",
                "9282744",
                "9282750",
                "9282767",
                "9282773",
                "9284499",
                "9284507",
                "9284513",
                "9284536",
                "9285381",
                "9285398",
                "9285406",
                "9285412",
                "9290459",
                "9290465",
                "9290471",
                "9296829",
                "9296835",
                "9296841",
                "9296858",
                "9299443",
                "9299466",
                "9299472",
                "9299489",
                "9305379",
                "9305385",
                "9305391",
                "9308254",
                "9308923",
                "9308946",
                "9308952",
                "9308969",
                "9319022",
                "9327903",
                "9327926",
                "9327932",
                "9327949",
                "9332809",
                "9332815",
                "9332821",
                "9363282",
                "9363299",
                "9363307",
                "9366441",
                "9366458",
                "9401350",
                "9401367",
            ],
        }

        # table T_MCOaaMED **not yet implemented**

        if nb is None or nb <= 0:
            self.nb = int(rd.uniform(3, 10))
        else:
            self.nb = nb

        self.delay = delay
        self.CT_code = None
        self.ucd_atc = None
        self.ucd = None

    def pathdesc(self, patient: Patient, d: date):
        # then, pick up a random UCD code, through the choice of an ATC code:
        self.ucd_atc = rd.choice(list(self.code_CT_ATC.keys()))
        self.ucd = rd.choice(self.code_CT_ATC[self.ucd_atc])

        # choose the type of medical act
        p = np.array(list(self.code_CT_CCAM.values()), dtype="float64")
        p /= np.sum(p)
        if self.CT_code is None:
            self.CT_code = rd.choice(list(self.code_CT_CCAM.keys()), p=p)
        oldd = d
        for _ in range(self.nb):
            care = ShortHospStay(patient, self.sim.etablissements[0], "Z511")
            care.DRel = "C50"
            if patient.raw_data['nodal_status']==1:
                care.cim_das.append("C773")
            care.ccam.append(self.CT_code)
            care.UCDs.append(ShortHospStay.UCD(self.ucd))
            if date.weekday(d) == 5:  # Saturday
                d += relativedelta(days=-1)
            elif date.weekday(d) == 6:  # Sunday
                d += relativedelta(days=1)
            care.start_date = d
            care.finish_date = d
            patient.append(care)
            oldd = d
            d = d + relativedelta(days=self.delay)
        return oldd


class CT_bi(pathwayInjector):
    """Chimiotherapy (CT) pathway, bitherapy

    In a bitherapy, the patient starts with one type of drugs for CT, and at some point
    of the therapy, the injected drug is changed.
    In our model, we change the ATC type of the drugs delivered to patient though CT sessions.
    """

    def __init__(self, sim: simulation, nb: int = None, delays: int = (14, 21)):
        super().__init__(sim)
        self.code_CT_CIM10 = ["Z511", "Z512"]
        self.code_CT_CCAM = {
            "ZZLF900": 826,
            "EBLA001": 19,
            "ABLB006": 0,
            "AFLB003": 77,
            "AFLB013": 76,
            "ECLF005": 0,
            "ECLF006": 0,
            "EELF004": 0,
            "EELF005": 0,
            "EDLF014": 0,
            "EDLF015": 0,
            "EDLF016": 5,
            "EDLF017": 0,
            "EDLF018": 0,
            "EDLF019": 0,
            "EDLF020": 0,
            "EDLF021": 0,
            "EBLF002": 0,
            "EBLF003": 0,
            "GGLB001": 0,
            "GGLB008": 0,
            "HPLB002": 0,
            "HPLB003": 4,
            "HPLB007": 0,
            "ZZLF004": 10,
        }
        self.code_CT_ATC = {
            "L01AA01": ["9031083", "9031077", "9031114"],
            #"L01CD02": ["9178082", "9178099"],
            "L01DB03": [
                "9145332",
                "9145361",
                "9284306",
                "9284312",
                "9284329",
                "9284335",
                "9284341",
                "9284358",
                "9284364",
                "9284370",
                "9284387",
                "9284393",
                "9284401",
                "9284418",
                "9284424",
                "9285257",
                "9285263",
                "9285286",
                "9285292",
                "9285300",
                "9285317",
                "9285323",
                "9285346",
                "9285352",
                "9293357",
                "9293363",
                "9293386",
                "9293392",
                "9304457",
                "9304463",
                "9304486",
                "9304492",
                "9305095",
                "9305103",
                "9305126",
                "9305132",
                "9306605",
                "9306611",
                "9306628",
                "9306634",
                "9306640",
                "9306657",
                "9306663",
                "9306686",
                "9313137",
                "9313143",
                "9313166",
                "9313172",
                "9313189",
                "9316058",
                "9316064",
                "9316070",
                "9319708",
                "9319714",
                "9328481",
                "9332637",
                "9332643",
                "9337913",
                "9337936",
                "9337942",
                "9337959",
                "9343724",
                "9343730",
                "9343747",
                "9343753",
            ],
            "L01BC02": [
                "9224505",
                "9030534",
                "9224505",
                "9303995",
                "9191527",
                "9191533",
                "9191556",
                "9192107",
                "9192113",
                "9192136",
                "9196163",
                "9196186",
                "9196192",
                "9196200",
                "9216925",
                "9227685",
                "9227691",
                "9227716",
                "9227722",
                "9247191",
                "9247216",
                "9247222",
                "9247239",
                "9257315",
                "9268276",
                "9268282",
                "9334866",
                "9334872",
                "9334889",
                "9334895",
                "9340950",
                "9340967",
                "9340973",
                "9340996",
                "9341004",
            ],
            "L01CD01": [
                "9278091",
                "9278116",
                "9278122",
                "9282744",
                "9282750",
                "9282767",
                "9282773",
                "9284499",
                "9284507",
                "9284513",
                "9284536",
                "9285381",
                "9285398",
                "9285406",
                "9285412",
                "9290459",
                "9290465",
                "9290471",
                "9296829",
                "9296835",
                "9296841",
                "9296858",
                "9299443",
                "9299466",
                "9299472",
                "9299489",
                "9305379",
                "9305385",
                "9305391",
                "9308254",
                "9308923",
                "9308946",
                "9308952",
                "9308969",
                "9319022",
                "9327903",
                "9327926",
                "9327932",
                "9327949",
                "9332809",
                "9332815",
                "9332821",
                "9363282",
                "9363299",
                "9363307",
                "9366441",
                "9366458",
                "9401350",
                "9401367",
            ],
        }

        if nb is None:
            self.nb = int(rd.uniform(3, 6))
        else:
            self.nb = nb

        # pick up a random UCD code, through the choice of an ATC code:
        self.ucd_atc = None
        self.ucd = None

        self.delays = delays
        self.CT_code = None

    def pathdesc(self, patient: Patient, d: date):

        # start by choosing the type of medical act for the sessions
        p = np.array(list(self.code_CT_CCAM.values()), dtype="float64")
        p /= np.sum(p)
        if self.CT_code is None:
            self.CT_code = rd.choice(list(self.code_CT_CCAM.keys()), p=p)

        # then, pick up a random UCD code, through the choice of an ATC code:
        self.ucd_atc = rd.choice(list(self.code_CT_ATC.keys()))
        self.ucd = rd.choice(self.code_CT_ATC[self.ucd_atc])

        oldd = d
        nb = int(rd.uniform(3, 5))
        for _ in range(nb):
            care = ShortHospStay(patient, self.sim.etablissements[0], "Z511")
            care.DRel = "C50"
            if patient.raw_data['nodal_status']==1:
                care.cim_das.append("C773")
            care.ccam.append(self.CT_code)
            care.UCDs.append(ShortHospStay.UCD(self.ucd))
            if date.weekday(d) == 5:  # Saturday
                d += relativedelta(days=-1)
            elif date.weekday(d) == 6:  # Sunday
                d += relativedelta(days=1)
            care.start_date = d
            care.finish_date = d
            patient.append(care)
            oldd = d
            d = d + relativedelta(days=self.delays[0])

        # change the type of ATC drugs (bitherapy)
        ucd_list = list(self.code_CT_ATC.keys())
        ucd_list.remove(self.ucd_atc)
        self.ucd_atc = rd.choice(ucd_list)
        self.ucd = rd.choice(self.code_CT_ATC[self.ucd_atc])
        nb = int(rd.uniform(2, 5))
        for _ in range(nb):
            care = ShortHospStay(patient, self.sim.etablissements[0], "Z511")
            care.DRel = "C50"
            if patient.raw_data['nodal_status']==1:
                care.cim_das.append("C773")
            care.ccam.append(self.CT_code)
            care.UCDs.append(ShortHospStay.UCD(self.ucd))
            if date.weekday(d) == 5:  # Saturday
                d += relativedelta(days=-1)
            elif date.weekday(d) == 6:  # Sunday
                d += relativedelta(days=1)
            care.start_date = d
            care.finish_date = d
            patient.append(care)
            oldd = d
            d = d + relativedelta(days=self.delays[1])

        return oldd


class TT(pathwayInjector):
    """Targeted Therapy pathway"""

    def __init__(self, sim: simulation, nb: int = 3):
        super().__init__(sim)
        self.code_TT_ATC = ["L01XC03"]  # L01XE07 L01XC13, L01XC03, L01XC14 ???
        self.code_TT_CIP13 = [
            "3400938379457",
            "3400938379518",
            "3400941701764",
            "3400941701825",
            "3400941701993",
            "3400949164349",
        ]

        # Code UCD pour TT: "select PHA_CIP_UCD from IR_PHA_R where PHA_EPH_COD LIKE "L01X*" ""
        # -> "select PHA_CIP_UCD from IR_PHA_R where PHA_ATC_C07 LIKE "L01XC03" ""
        self.nb = nb

    def pathdesc(self, patient: Patient, d: date):
        TT_code = rd.choice(self.code_TT_CIP13)
        for i in range(self.nb):
            care = DrugDelivery(TT_code, patient, self.sim.pharms[0])
            care.date_debut = d
            care.date_fin = d
            patient.append(care)
            if i != 2:
                d = d + relativedelta(weeks=3)
        return d


# implementation of a breast cancer pathway (P1)
class P1(pathwayInjector):
    """
    The starting date of this care pathway is not triggered by an
    external event: it is randomly generated !
    """

    def pathdesc(self, patient: Patient, d: date):

        if len(self.sim.etablissements) == 0:
            warnings.warn(
                "Warning (Pathway): no etablissement available to create the pathway"
            )
            return

        d = Surgery(self.sim).pathdesc(patient, d)
        return d

    def __precondition__(self, patient: Patient) -> bool:
        return patient.raw_data["pathway"] == "P1"

    def __index_date__(self, patient: Patient, d: date = pd.NaT):
        return date(day=1, month=1, year=self.sim.year) + relativedelta(
            days=int(rd.uniform(0, 364))
        )


class P2(pathwayInjector):
    """Implementation of a breast cancer pathway (P2)

    This profil corresponds to patients having a treatment pathway with [S+ET].

    The starting date of this care pathway is uniformly random generated within the simulation year.
    """

    def pathdesc(self, patient: Patient, d: date):
        if len(self.sim.etablissements) == 0:
            warnings.warn(
                "Warning (Pathway): no etablissement available to create the pathway"
            )
            return

        d = Surgery(self.sim).pathdesc(patient, d)

        # some days later
        d = d + relativedelta(days=int(rd.uniform(3, 31)))

        # inject ET
        d = ET_uni(self.sim).pathdesc(patient, d)
        return d

    def __precondition__(self, patient: Patient) -> bool:
        return patient.raw_data["pathway"] == "P2"

    def __index_date__(self, patient: Patient, d: date = pd.NaT):
        return date(day=1, month=1, year=self.sim.year) + relativedelta(
            days=int(rd.uniform(0, 364))
        )


class P3(pathwayInjector):
    """Implementation of a breast cancer pathway (P3)

    This profil corresponds to patients having a treatment pathway with [S+RT].

    The starting date of this care pathway is uniformly random generated within the simulation year.
    """

    def pathdesc(self, patient: Patient, d: date):
        if len(self.sim.etablissements) == 0:
            warnings.warn(
                "Warning (Pathway): no etablissement available to create the pathway"
            )
            return

        d = Surgery(self.sim).pathdesc(patient, d)

        # some days later
        d = d + relativedelta(days=int(rd.uniform(3, 31)))

        # Radiotherapy
        d = RT(self.sim).pathdesc(patient, d)
        return d

    def __precondition__(self, patient: Patient) -> bool:
        return patient.raw_data["pathway"] == "P3"

    def __index_date__(self, patient: Patient, d: date = pd.NaT):
        return date(day=1, month=1, year=self.sim.year) + relativedelta(
            days=int(rd.uniform(0, 364))
        )


class P4(pathwayInjector):
    """Implementation of a breast cancer pathway (P4)

    This profil corresponds to patients having a treatment pathway with [S+RT+ET].

    The starting date of this care pathway is uniformly random generated within the simulation year.
    """

    def pathdesc(self, patient: Patient, d: date):
        if len(self.sim.etablissements) == 0:
            warnings.warn(
                "Warning (Pathway): no etablissement available to create the pathway"
            )
            return

        d = Surgery(self.sim).pathdesc(patient, d)

        # some days later
        d = d + relativedelta(days=int(rd.uniform(3, 31)))

        # Radiotherapy
        d = RT(self.sim).pathdesc(patient, d)

        # some days later
        d = d + relativedelta(days=int(rd.uniform(3, 31)))

        # ET
        d = ET_uni(self.sim).pathdesc(patient, d)
        return d

    def __precondition__(self, patient: Patient) -> bool:
        return patient.raw_data["pathway"] == "P4"

    def __index_date__(self, patient: Patient, d: date = pd.NaT):
        return date(day=1, month=1, year=self.sim.year) + relativedelta(
            days=int(rd.uniform(0, 364))
        )


class P5(pathwayInjector):
    """Implementation of a breast cancer pathway (P5)

    This profil corresponds to patients having a treatment pathway with [CT+S+RT+ET].

    The starting date of this care path
    """

    def pathdesc(self, patient: Patient, d: date):
        if len(self.sim.etablissements) == 0:
            warnings.warn(
                "Warning (Pathway): no etablissement available to create the pathway"
            )
            return

        age = patient.age(d)
        if age < 50:
            unict = rd.binomial(1, 0.1667)
        elif age <= 59:
            unict = rd.binomial(1, 0.1333)
        elif age <= 69:
            unict = rd.binomial(1, 0.1)
        else:
            unict = 0 #no patients here

        if unict:
            # CT unitherapy
            d = CT_uni(self.sim).pathdesc(patient, d)
        else:
            # CT bitherapy
            d = CT_bi(self.sim).pathdesc(patient, d)

        # some days later
        d = d + relativedelta(days=int(rd.uniform(3, 16)))
        # S
        d = Surgery(self.sim).pathdesc(patient, d)

        # some days later
        d = d + relativedelta(days=int(rd.uniform(15, 61)))

        # Radiotherapy
        d = RT(self.sim).pathdesc(patient, d)

        # some days later
        d = d + relativedelta(days=int(rd.uniform(3, 31)))

        # ET
        d = ET_uni(self.sim).pathdesc(patient, d)
        return d

    def __precondition__(self, patient: Patient) -> bool:
        return patient.raw_data["pathway"] == "P5"

    def __index_date__(self, patient: Patient, d: date = pd.NaT):
        return date(day=1, month=1, year=self.sim.year) + relativedelta(
            days=int(rd.uniform(0, 364))
        )


class P6(pathwayInjector):
    """Implementation of a breast cancer pathway (P6)

    This profil corresponds to patients having a treatment pathway with [CT+S+RT].

    The starting date of this care pathway is uniformly random generated within the simulation year.
    """

    def pathdesc(self, patient: Patient, d: date):
        if len(self.sim.etablissements) == 0:
            warnings.warn(
                "Warning (Pathway): no etablissement available to create the pathway"
            )
            return

        age = patient.age(d)
        if age < 50:
            unict = rd.binomial(1, 0.143)
        elif age <= 59:
            unict = rd.binomial(1, 0.1667)
        elif age <= 69:
            unict = rd.binomial(1, 0.75)
        else:
            unict = rd.binomial(1, 1)

        if unict:
            # CT unitherapy
            d = CT_uni(self.sim).pathdesc(patient, d)
        else:
            # CT bitherapy
            d = CT_bi(self.sim).pathdesc(patient, d)

        # some days later
        d = d + relativedelta(days=int(rd.uniform(16, 61)))

        # S
        d = Surgery(self.sim).pathdesc(patient, d)

        # some days later
        d = d + relativedelta(days=int(rd.uniform(15, 61)))

        # Radiotherapy
        d = RT(self.sim).pathdesc(patient, d)
        return d

    def __precondition__(self, patient: Patient) -> bool:
        return patient.raw_data["pathway"] == "P6"

    def __index_date__(self, patient: Patient, d: date = pd.NaT):
        return date(day=1, month=1, year=self.sim.year) + relativedelta(
            days=int(rd.uniform(0, 364))
        )


# implementation of a breast cancer pathway (P7)
class P7(pathwayInjector):
    """Implementation of a breast cancer pathway (P7)

    This profil corresponds to patients having a treatment pathway with [S+RT].

    The starting date of this care pathway is uniformly random generated within the simulation year.

    """

    def __init__(self, sim: simulation):
        super().__init__(sim)
        # self.code_TT_ATC = ["L01XC03"] # code pour Herceptin
        self.code_TT_UCD = ["9220097", "9400037"]  # version UCD de l'Herceptin

    def pathdesc(self, patient: Patient, d: date):
        """
        - d is the starting date of the pathway
        """

        if len(self.sim.etablissements) == 0:
            warnings.warn(
                "Warning (Pathway): no etablissement available to create the pathway"
            )
            return
        if len(self.sim.pharms) == 0:
            warnings.warn(
                "Warning (Pathway): no drugstore available to create the pathway"
            )
            return

        age = patient.age(d)
        if age < 50:
            unict = rd.binomial(1, 0.143)
        elif age <= 59:
            unict = rd.binomial(1, 0.6667)
        elif age <= 69:
            unict = rd.binomial(1, 0.75)
        else:
            unict = rd.binomial(1, 1)

        # init the treatment with 4 sessions of CT alone
        therapy_phase1 = CT_uni(self.sim, nb=4, delay=15)
        d = therapy_phase1.pathdesc(patient, d)

        # CT continue in association with TT
        TT_code = rd.choice(self.code_TT_UCD)
        if unict:
            # CT unitherapy
            therapy = CT_uni(self.sim)
            therapy.CT_code = therapy_phase1.CT_code

            for i in range(12):
                care = ShortHospStay(patient, self.sim.etablissements[0], "Z511")
                care.DRel = "Z50"
                if patient.raw_data['nodal_status']==1:
                    care.cim_das.append("C773")
                if date.weekday(d) == 5:  # Saturday
                    d += relativedelta(days=-1)
                elif date.weekday(d) == 6:  # Sunday
                    d += relativedelta(days=1)
                care.start_date = d
                care.finish_date = d
                care.ccam.append(therapy_phase1.CT_code)  # same codes as before for CT
                care.UCDs.append(ShortHospStay.UCD(therapy_phase1.ucd))
                if i % 3 == 0:
                    care.UCDs.append(ShortHospStay.UCD(TT_code))
                patient.append(care)
                d = d + relativedelta(days=7)
            d = d - relativedelta(days=7)
        else:
            # bi-therapy
            nb = int(rd.uniform(1, 10))
            i = 0
            while i < nb:
                care = ShortHospStay(patient, self.sim.etablissements[0], "Z511")
                care.DRel = "C50"
                if patient.raw_data['nodal_status']==1:
                    care.cim_das.append("C773")
                care.ccam.append(therapy_phase1.CT_code)
                care.UCDs.append(ShortHospStay.UCD(therapy_phase1.ucd))
                if i % 3 == 0:  # combinaison with TT
                    care.UCDs.append(ShortHospStay.UCD(TT_code))
                if date.weekday(d) == 5:  # Saturday
                    d += relativedelta(days=-1)
                elif date.weekday(d) == 6:  # Sunday
                    d += relativedelta(days=1)
                care.start_date = d
                care.finish_date = d
                patient.append(care)
                d = d + relativedelta(days=7)
                i += 1

            # choose another drug code for CT (bitherapy)
            ucd_list = list(therapy_phase1.code_CT_ATC.keys())
            ucd_list.remove(therapy_phase1.ucd_atc)
            ucd_atc = rd.choice(ucd_list)
            ucd = rd.choice(therapy_phase1.code_CT_ATC[ucd_atc])
            while i < 12:
                care = ShortHospStay(patient, self.sim.etablissements[0], "Z511")
                care.DRel = "C50"
                if patient.raw_data['nodal_status']==1:
                    care.cim_das.append("C773")
                care.ccam.append(therapy_phase1.CT_code)
                care.UCDs.append(ShortHospStay.UCD(ucd))
                if date.weekday(d) == 5:  # Saturday
                    d += relativedelta(days=-1)
                elif date.weekday(d) == 6:  # Sunday
                    d += relativedelta(days=1)
                care.start_date = d
                care.finish_date = d
                if i % 3 == 0:
                    care.UCDs.append(ShortHospStay.UCD(TT_code))
                patient.append(care)
                d = d + relativedelta(days=7)
                i += 1
            d = d - relativedelta(days=7)

        # Surgery
        dch = d + relativedelta(days=int(rd.uniform(7, 21)))
        Surgery(self.sim).pathdesc(patient, dch)

        # break
        d = dch + relativedelta(days=int(rd.uniform(3, 31)))

        # TT
        d = TT(self.sim, nb=10).pathdesc(patient, d)

        # restart from surgery, not synchronized
        d = dch + relativedelta(days=int(rd.uniform(3, 31)))

        # RT
        d = RT(self.sim).pathdesc(patient, d)

        # break after RT
        d = d + relativedelta(days=int(rd.uniform(3, 31)))

        # ET bitherapy
        d = ET_bi(self.sim).pathdesc(patient, d)
        return d

    def __precondition__(self, patient: Patient) -> bool:
        return patient.raw_data["pathway"] == "P7"

    def __index_date__(self, patient: Patient, d: date = pd.NaT):
        return date(day=1, month=1, year=self.sim.year) + relativedelta(
            days=int(rd.uniform(0, 364))
        )


class P8(pathwayInjector):
    """Implementation of a breast cancer pathway (P8)

    This profil corresponds to patients having a treatment pathway with [S+CT+TT+RT+ET].

    The starting date of this care pathway is uniformly random generated within the simulation year.

    """

    def __init__(self, sim: simulation):
        super().__init__(sim)
        self.code_TT_ATC = ["L01XC03"]  # L01XE07 L01XC13, L01XC03, L01XC14 ???
        self.code_TT_CIP13 = [
            "3400938379457",
            "3400938379518",
            "3400941701764",
            "3400941701825",
            "3400941701993",
            "3400949164349",
        ]
        self.code_TT_UCD = ["9220097", "9400037"]  # version UCD de l'Herceptin

    def pathdesc(self, patient: Patient, d: date):
        """
        - d is the starting date of the pathway
        """

        if len(self.sim.etablissements) == 0:
            warnings.warn(
                "Warning (Pathway): no etablissement available to create the pathway"
            )
            return
        if len(self.sim.pharms) == 0:
            warnings.warn(
                "Warning (Pathway): no drugstore available to create the pathway"
            )
            return

        # Surgery
        d = Surgery(self.sim).pathdesc(patient, d)

        d = d + relativedelta(days=int(rd.uniform(15, 61)))

        # Chimiotherapy
        age = patient.age(d)
        if age < 50:
            unict = rd.binomial(1, 0.25)
        elif age <= 59:
            unict = rd.binomial(1, 0.6667)
        elif age <= 69:
            unict = rd.binomial(1, 0.214)
        else:
            unict = 1

        # init the treatment with 4 sessions of CT alone
        therapy_phase1 = CT_uni(self.sim, nb=4, delay=15)
        d = therapy_phase1.pathdesc(patient, d)

        # CT continue in association with TT
        TT_code = rd.choice(self.code_TT_UCD)
        if unict:
            # CT unitherapy
            therapy = CT_uni(self.sim)
            therapy.CT_code = therapy_phase1.CT_code

            for i in range(12):
                care = ShortHospStay(patient, self.sim.etablissements[0], "Z511")
                care.DRel = "C50"
                if patient.raw_data['nodal_status']==1:
                    care.cim_das.append("C773")
                if date.weekday(d) == 5:  # Saturday
                    d += relativedelta(days=-1)
                elif date.weekday(d) == 6:  # Sunday
                    d += relativedelta(days=1)
                care.start_date = d
                care.finish_date = d
                care.ccam.append(therapy_phase1.CT_code)  # same codes as before for CT
                care.UCDs.append(ShortHospStay.UCD(therapy_phase1.ucd))
                if i % 3 == 0:
                    care.UCDs.append(ShortHospStay.UCD(TT_code))
                patient.append(care)
                d = d + relativedelta(days=7)
            d = d - relativedelta(days=7)
        else:
            # bi-therapy
            nb = int(rd.uniform(1, 10))
            i = 0
            while i < nb:
                care = ShortHospStay(patient, self.sim.etablissements[0], "Z511")
                care.DRel = "C50"
                if patient.raw_data['nodal_status']==1:
                    care.cim_das.append("C773")
                care.ccam.append(therapy_phase1.CT_code)
                care.UCDs.append(ShortHospStay.UCD(therapy_phase1.ucd))
                if i % 3 == 0:  # combinaison with TT
                    care.UCDs.append(ShortHospStay.UCD(TT_code))
                if date.weekday(d) == 5:  # Saturday
                    d += relativedelta(days=-1)
                elif date.weekday(d) == 6:  # Sunday
                    d += relativedelta(days=1)
                care.start_date = d
                care.finish_date = d
                patient.append(care)
                d = d + relativedelta(days=7)
                i += 1

            # choose another drug code for CT (bitherapy)
            ucd_list = list(therapy_phase1.code_CT_ATC.keys())
            ucd_list.remove(therapy_phase1.ucd_atc)
            ucd_atc = rd.choice(ucd_list)
            ucd = rd.choice(therapy_phase1.code_CT_ATC[ucd_atc])
            while i < 12:
                care = ShortHospStay(patient, self.sim.etablissements[0], "Z511")
                care.DRel = "C50"
                if patient.raw_data['nodal_status']==1:
                    care.cim_das.append("C773")
                care.ccam.append(therapy_phase1.CT_code)
                care.UCDs.append(ShortHospStay.UCD(ucd))
                if date.weekday(d) == 5:  # Saturday
                    d += relativedelta(days=-1)
                elif date.weekday(d) == 6:  # Sunday
                    d += relativedelta(days=1)
                care.start_date = d
                care.finish_date = d
                if i % 3 == 0:
                    care.UCDs.append(ShortHospStay.UCD(TT_code))
                patient.append(care)
                d = d + relativedelta(days=7)
                i += 1
            d = d - relativedelta(days=7)

        # break
        d = d + relativedelta(days=int(rd.uniform(3, 31)))

        # RT
        d = RT(self.sim).pathdesc(patient, d)

        # break
        d = d + relativedelta(days=int(rd.uniform(3, 31)))

        # ET bitherapy
        d = ET_bi(self.sim).pathdesc(patient, d)

        return d

    def __precondition__(self, patient: Patient) -> bool:
        return patient.raw_data["pathway"] == "P8"

    def __index_date__(self, patient: Patient, d: date = pd.NaT):
        return date(day=1, month=1, year=self.sim.year) + relativedelta(
            days=int(rd.uniform(0, 364))
        )


class P9(pathwayInjector):
    """Implementation of a breast cancer pathway (P9)

    This profil corresponds to patients having a treatment pathway with [S+CT+RT].

    The starting date of this care pathway is uniformly random generated within the simulation year.

    """

    def pathdesc(self, patient: Patient, d: date):
        """
        - d is the starting date of the pathway
        """

        if len(self.sim.etablissements) == 0:
            warnings.warn(
                "Warning (Pathway): no etablissement available to create the pathway"
            )
            return
        if len(self.sim.pharms) == 0:
            warnings.warn(
                "Warning (Pathway): no drugstore available to create the pathway"
            )
            return

        # Surgery
        d = Surgery(self.sim).pathdesc(patient, d)

        d = d + relativedelta(days=int(rd.uniform(15, 61)))

        # Chimiotherapy
        age = patient.age(d)
        if age < 50:
            unict = rd.binomial(1, 0.25)
        elif age <= 59:
            unict = rd.binomial(1, 0.65)
        elif age <= 69:
            unict = rd.binomial(1, 0.7778)
        else:
            unict = 1

        if unict:
            # CT unitherapy
            d = CT_uni(self.sim).pathdesc(patient, d)
        else:
            # CT bitherapy
            d = CT_bi(self.sim).pathdesc(patient, d)

        # break
        d = d + relativedelta(days=int(rd.uniform(3, 31)))

        # RT
        d = RT(self.sim).pathdesc(patient, d)
        return d

    def __precondition__(self, patient: Patient) -> bool:
        return patient.raw_data["pathway"] == "P9"

    def __index_date__(self, patient: Patient, d: date = pd.NaT):
        return date(day=1, month=1, year=self.sim.year) + relativedelta(
            days=int(rd.uniform(0, 364))
        )


class P10(pathwayInjector):
    """Implementation of a breast cancer pathway (P10)

    This profil corresponds to patients having a treatment pathway with [S+CT+RT+ET].

    The starting date of this care pathway is uniformly random generated within the simulation year.
    """

    def pathdesc(self, patient: Patient, d: date):
        """
        - d is the starting date of the pathway
        """

        if len(self.sim.etablissements) == 0:
            warnings.warn(
                "Warning (Pathway): no etablissement available to create the pathway"
            )
            return
        if len(self.sim.pharms) == 0:
            warnings.warn(
                "Warning (Pathway): no drugstore available to create the pathway"
            )
            return

        # Surgery
        d = Surgery(self.sim).pathdesc(patient, d)

        d = d + relativedelta(days=int(rd.uniform(15, 61)))

        # Chimiotherapy
        age = patient.age(d)
        if age < 50:
            unict = rd.binomial(1, 0.236)
        elif age <= 59:
            unict = rd.binomial(1, 0.654)
        elif age <= 69:
            unict = rd.binomial(1, 0.796)
        else:
            unict = 1

        if unict:
            # CT unitherapy
            d = CT_uni(self.sim).pathdesc(patient, d)
        else:
            # CT bitherapy
            d = CT_bi(self.sim).pathdesc(patient, d)

        # break
        d = d + relativedelta(days=int(rd.uniform(3, 31)))

        # RT
        d = RT(self.sim).pathdesc(patient, d)

        # break
        d = d + relativedelta(days=int(rd.uniform(3, 31)))

        # ET bitherapy
        if age < 50:
            uniet = rd.binomial(1, 0.945)
        elif age <= 59:
            uniet = rd.binomial(1, 0.692)
        elif age <= 69:
            uniet = rd.binomial(1, 0.87)
        else:
            uniet = rd.binomial(1, 0.5833)

        if uniet:
            # ET unitherapy
            d = ET_uni(self.sim).pathdesc(patient, d)
        else:
            # ET bitherapy
            d = ET_bi(self.sim).pathdesc(patient, d)

        return d

    def __precondition__(self, patient: Patient) -> bool:
        return patient.raw_data["pathway"] == "P10"

    def __index_date__(self, patient: Patient, d: date = pd.NaT):
        return date(day=1, month=1, year=self.sim.year) + relativedelta(
            days=int(rd.uniform(0, 364))
        )
