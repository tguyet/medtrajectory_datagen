
##### Simulation âge


import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import truncnorm
import seaborn as sns
import math


### Profil 1

# Nombre total de points à générer
total_points = 10500

# Définition des proportions pour chaque tranche d'âge
proportions = [0.2143, 0.2571, 0.2857, 0.2429]

# Ajuster les paramètres pour les lois normales tronquées
# (moyenne, écart-type, borne inférieure, borne supérieure)
params = [
    (48, 7, 20, 49),   # <50 ans
    (54, 3.5, 50, 59), # 50-59 ans
    (65, 4, 60, 69),   # 60-69 ans
    (75, 6, 70, 95)    # ≥70 ans
]

# Calcul des effectifs en arrondissant à l'entier le plus proche
n_points_per_group = [int(round(total_points * prop)) for prop in proportions]

# Ajuster le dernier groupe pour compenser l'écart
n_points_per_group[-1] += total_points - sum(n_points_per_group)

# Fonction pour générer les données en utilisant une loi normale tronquée
def generate_truncated_normal(mean, std, lower, upper, n):
    a, b = (lower - mean) / std, (upper - mean) / std
    return truncnorm.rvs(a, b, loc=mean, scale=std, size=n)

# Génération des données
samples1 = []
age_labels = []
for i, (n_points, (mean, std, lower, upper)) in enumerate(zip(n_points_per_group, params)):
    samples1.extend(generate_truncated_normal(mean, std, lower, upper, n_points))
    age_labels.extend([i] * n_points)

# Convertir en numpy array pour manipulation
samples1 = np.array(np.round(samples1))
age_labels = np.array(age_labels)

# Définir les couleurs pour chaque tranche d'âge
colors = ['blue', 'orange', 'green', 'red']

# Tracer l'histogramme avec des couleurs différentes pour chaque tranche d'âge
plt.figure(figsize=(9, 6))
for i, (color, (mean, std, lower, upper)) in enumerate(zip(colors, params)):
    sns.histplot(samples1[age_labels == i], bins=range(20, 101), kde=False, color=color, edgecolor='black', alpha=0.5, label=f'Tranche {i+1}: {lower}-{upper} ans')

# Superposer les densités pour chaque tranche d'âge avec la couleur correspondante
bin_width = 1  # Les bins ont une largeur de 1
for i, (color, (mean, std, lower, upper)) in enumerate(zip(colors, params)):
    a, b = (lower - mean) / std, (upper - mean) / std
    x = np.linspace(lower, upper, 1000)
    # Mettre à l'échelle la densité pour correspondre à la fréquence
    density = truncnorm.pdf(x, a, b, loc=mean, scale=std) * (n_points_per_group[i]) * bin_width
    plt.plot(x, density, lw=3.5, color=color, label=f'Densité tranche {i+1}')

plt.xlabel('Âge au diagnostic')
plt.ylabel('Fréquence')
plt.legend()
plt.show()

# Vérification des proportions
print('Total d echantillons pour la population 1 : ' + str(len(samples1)))
print('Vérification des statistiques :')
print('Proportion de <50 ans : ' + str(round((len(np.where(samples1<50)[0])/len(samples1))*100, 2)))
print('Proportion de 50-59 ans : ' + str(round((np.sum((samples1 >= 50) & (samples1 <= 59))/len(samples1))*100, 2)))
print('Proportion de 60-69 ans : ' + str(round((np.sum((samples1 >= 60) & (samples1 <= 69))/len(samples1))*100, 2)))
print('Proportion de >70 ans : ' + str(round((len(np.where(samples1>=70)[0])/len(samples1))*100, 2)))


