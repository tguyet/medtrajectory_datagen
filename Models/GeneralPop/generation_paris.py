#!/usr/bin/python3
# -*- coding: utf-8 -*-
import simulationDB
import numpy as np

np.random.seed(4218)

for dbi in range(10):
	sim = simulationDB.OpenSimulation(nomencl="/home/tguyet/Progs/medtrajectory_datagen/Generator/snds_nomenclature.db", datarep="/home/tguyet/Progs/medtrajectory_datagen/datarep")
	sim.nb_patients=10000 #all
	sim.nb_physicians=1000 #all
	sim.dpts=[75,77,78,91,92,93,94,95]

	sim.verbose=True

	sim.run()
	dbgen = simulationDB.simDB()
	dbgen.output_db_name="snds_Paris_db%d.db"%dbi
	dbgen.generate(sim, rootschemas="../external/schema-snds-master/schemas")


