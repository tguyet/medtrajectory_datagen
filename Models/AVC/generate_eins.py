#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: Thomas Guyet, Inria
@date 09/07/2023
"""

from optparse import Values
import pandas as pd
import numpy as np
import pickle

from SNDSGenerator.data_factory import PharmacyFactory, DrugsDeliveryFactory
from SNDSGenerator.data_factory import FactoryContext, PatientFactory
from SNDSGenerator.simu_open import OpenShortStayFactory, OpenDataFactoryContext

from pathway_avc import AVCPathwayInjector, GenEpiPathwayInjector

from SNDSGenerator.simuExternal import simuExternal
from SNDSGenerator.simulationDB import simDB

import logging

logging.basicConfig(filename="debug.log", encoding="utf-8", level=logging.DEBUG)

#### GENERATION of a POPULATION

taille_pop = 100

##Generate a population
pop = pd.read_csv("datarep/pop.csv").drop(["Unnamed: 0"], axis=1)
pop = pop.dropna()
tot_pop = np.sum(pop["pop"])
pop["pop"] /= tot_pop

population = (
    pop.iloc[np.random.choice(len(pop["pop"]), p=pop["pop"], size=taille_pop)]
    .drop(["pop"], axis=1)
    .reset_index(drop=True)
)
population.reset_index(inplace=True)
population.rename(columns={"index": "id"}, inplace=True)

###### Assign an AVC status

pAVC, pTA = pickle.load(open("Models/AVC/ptavc.pkl", "rb"))
pAVC["sex"] = (pAVC["gender"] == "F").astype("int") + 1

# transform the age into intervals matching the ones of pTAVC
population["ageinter"] = pd.cut(population.age, pd.IntervalIndex(pAVC.age.unique()))
population = pd.merge(
    population,
    pAVC,
    how="left",
    left_on=["ageinter", "sex", "dpt"],
    right_on=["age", "sex", "Code"],
    suffixes=("", "_p"),
)[["id", "age", "sex", "dpt", "code", "RR", "city_name", "p"]]


population["avc"] = population["p"] > np.random.rand(len(population["p"]))
population["type"] = np.random.choice(
    pTA["type"], p=pTA["p"], size=len(population["p"])
)
population.loc[population.avc == False, "type"] = np.nan

nbavc = len(population.loc[population.avc == True])
population.loc[population.avc == True, "date_avc"] = [
    np.datetime64("2022-01-01") + np.timedelta64(e, "D")
    for e in np.random.randint(size=nbavc, low=0, high=365)
]

########################
# uniform generation of genepi population (with strokes)
population["genepi"] = np.random.rand(len(population)) < (1000 / taille_pop)
nbgenepi = len(population.loc[population.genepi == True])
population.loc[population.genepi == True, "date_genepi"] = [
    np.datetime64("2022-01-01") + np.timedelta64(e, "D")
    for e in np.random.randint(size=nbgenepi, low=183, high=365)
]
########################


mortality_laws = pickle.load(open("Models/AVC/mortality_weibull_params.pkl", "rb"))
# mortality_laws[['age','sex','type','k','lambda']]
mortality_laws.set_index(["age", "sex", "type"], inplace=True)  # ,'k','lambda']]

malades = population[population.avc == True]
malades["ageinter"] = pd.cut(malades.age, pd.IntervalIndex(pAVC.age.unique()))
malades = pd.merge(
    malades,
    mortality_laws,
    how="left",
    left_on=["ageinter", "sex", "type"],
    right_on=["age", "sex", "type"],
    suffixes=("", "_x"),
)

if len(malades) != 0:
    rng = np.random.default_rng()
    malades["deathdate"] = (malades["lambda"] * rng.weibull(malades["k"])).astype("int")
    malades.loc[malades["deathdate"] < 0, "deathdate"] = 0
    malades.loc[malades["deathdate"] >= 366, "deathdate"] = np.nan
    malades.loc[malades["deathdate"] < 366, "deathdate"] = malades.loc[
        malades["deathdate"] < 366, "date_avc"
    ] + pd.to_timedelta(malades.loc[malades["deathdate"] < 366, "deathdate"], "D")
    malades = malades[["id", "deathdate"]]

    # add ALD1 (AVC invalidant) ...
    aldcim_values = ["G81", "I61", "I63", "I64", "I69", None]
    p_none = 0.4  # assume 60% of strokes events
    malades["aldcim"] = [
        aldcim_values[c]
        for c in rng.choice(
            6,
            p=[
                (1 - p_none) * 0.14,
                (1 - p_none) * 0.04,
                (1 - p_none) * 0.13,
                (1 - p_none) * 0.54,
                (1 - p_none) * 0.15,
                p_none,
            ],
            size=len(malades),
        )
    ]

    population = pd.merge(population, malades, how="left", on=["id"])

    population["deathdate"] = pd.to_datetime(population["deathdate"])

else:
    population["deathdate"] = pd.to_datetime(np.nan)
    population["aldcim"] = np.NaN

logging.debug("Number of illed patients: %d" % len(malades))
logging.debug(
    "Number of dead patients: %d"
    % sum(population["deathdate"] != pd.to_datetime(np.nan))
)

## Now ... we generate a database using these datasets


simulator = simuExternal(nomencl="datarep/snds_nomenclature.db", datarep="datarep")
simulator.run()


# create a context with the default nomenclature DB
# context =  OpenDataFactoryContext("datarep/snds_nomenclature.db", datarep="../datarep")
# generate the patients
factory = PatientFactory(simulator.context, simulator.GPs)
attribute_map = {"sex": "sex", "age": "age", "dc_date": "deathdate", "dc_cause": "type"}
patients = factory.load(population, attribute_map)


##########################################
## GENERATION of DRUGS DELIVERIES
##########################################

# Generate some random drug deliveries
drugs_freq = pd.read_csv("datarep/drugs_freq.csv").drop(["Unnamed: 0"], axis=1)
nb_deliveries = pd.read_csv("datarep/mean_deliveries.csv").drop(["Unnamed: 0"], axis=1)

# AVC_patients=population[population.avc==True]

# generation to do
to_generate = (
    population[["age", "sex", "RR", "avc"]]
    .groupby(["age", "sex", "RR", "avc"])
    .size()
    .reset_index()
    .rename(columns={0: "nb"})
)
to_generate = pd.merge(to_generate, nb_deliveries, on=["age", "sex", "RR"])
to_generate["nb"] = np.round(to_generate["nb"] * to_generate["mean"])
to_generate.drop("mean", axis=1, inplace=True)
to_generate = to_generate[to_generate["nb"] != 0]


# for a given set of characteristics to generate drugs
def generate_drugs(x):
    age = x["age"]
    sex = x["sex"]
    RR = x["RR"]
    avc = x["avc"]
    nb = int(x["nb"])

    possible_drugs = drugs_freq[
        (drugs_freq["age"] == age)
        & (drugs_freq["sex"] == sex)
        & (drugs_freq["RR"] == RR)
    ][["CIP13", "p"]]
    if len(possible_drugs) == 0:
        return None
    possible_drugs["p"] /= np.sum(possible_drugs["p"])

    loc_drugs = (
        possible_drugs.iloc[
            np.random.choice(len(possible_drugs), p=possible_drugs["p"], size=nb)
        ]
        .drop(["p"], axis=1)
        .reset_index(drop=True)
    )

    # list of the people having the characteristics
    indices = population[
        (population["age"] == age)
        & (population["sex"] == sex)
        & (population["RR"] == RR)
    ].reset_index()["index"]
    loc_drugs["patient"] = np.random.choice(indices, size=len(loc_drugs))

    # random dates
    loc_drugs["date"] = [
        np.datetime64("2022-01-01") + np.timedelta64(e, "D")
        for e in np.random.randint(size=len(loc_drugs), low=0, high=365)
    ]
    return loc_drugs


res = to_generate.apply(generate_drugs, axis=1)
drugs = pd.concat(res.to_list())

hosps = population[["id", "type", "date_avc"]].dropna()

# Add hospitalisation for epileptic patients
hosps_genepi = population[["id", "date_genepi"]].dropna()
hosps_genepi["type"] = np.random.choice(["G40", "G41"], size=len(hosps_genepi))
hosps_genepi = hosps_genepi.rename(columns={"date_genepi": "date_avc"})
hosps = pd.concat([hosps, hosps_genepi])

#################################################################
################################################################
# representation of a simulation with datasets
simu = {
    "patients": {
        "data": population,
        "attributes": {
            "sex": "sex",
            "age": "age",
            "dc_date": "deathdate",
            "dc_cause": "type",
            "ald_cimcode": "aldcim",
            "ald_date": "date_avc",
        },
    },
    "hosps": {
        "data": hosps,
        "attributes": {"pid": "id", "cim": "type", "date": "date_avc"},
    },
    "drugs": {
        "data": drugs,
        "attributes": {"pid": "patient", "cip": "CIP13", "date": "date"},
    },
}

simulator.load(simu)

# generate hospital stays randomly
factory = OpenShortStayFactory(simulator.context, simulator.etablissements)
# modify the factory to prevent generating other events than AVC events
factory.cims = list(
    filter(
        lambda item: not item.startswith("I60")
        and not item.startswith("I61")
        and not item.startswith("I62")
        and not item.startswith("I63")
        and not item.startswith("I64"),
        factory.cims,
    )
)

for p in simulator.patients:
    factory.generate(p)

simulator.context.year = 2022
injector = AVCPathwayInjector(simulator)
injector_genepi = GenEpiPathwayInjector(simulator)
for p in simulator.patients:
    injector.injection(p)
    injector_genepi.injection(p)

dbgen = simDB()
dbgen.output_db_name = "Models/AVC/snds_testgen.db"
dbgen.generate(simulator, rootschemas="schema-snds/schemas")
