from SNDSGenerator.pathway_factory import pathwayInjector
import pandas as pd
from SNDSGenerator.database_model import Patient, DrugDelivery, MedicalVisit
from SNDSGenerator.simulation import simulation

from datetime import date
from dateutil.relativedelta import relativedelta

import warnings
import pickle
import numpy.random as rd



class AVCPathwayInjector(pathwayInjector):
    def __init__(self, sim: simulation):
        super().__init__(sim)

    def pathdesc(self, patient: Patient, d: date):
        """"""

        if len(self.sim.pharms) == 0:
            warnings.warn(
                "Warning (AVCPathway): no pharmacies available to create the pathway"
            )
            return

        random = rd.default_rng()

        #cur = self.sim.context.conn.cursor()
        #cur.execute("select PHA_CIP_C13 from IR_PHA_R where PHA_MED_NOM like 'CLOPI%';")
        #codes_medoc = cur.fetchall()
        #cur.close()
        #codes_medoc = [c[0] for c in codes_medoc]
        #assert len(codes_medoc) > 0

        #cip = codes_medoc[0]
        #print(f"===> Injection of code '{cip}' to {patient.NIR}:")

        CIP_distrib_B01AC=pickle.load(open("AVCGenerator/cip13_B01AC.pkl", 'rb'))
        cip = CIP_distrib_B01AC.sample(weights="BOITES", replace=True).index[0]
        print(f"===> Injection of code '{cip}' to {patient.NIR}:")

        dd = date(d.year, d.month, d.day)
        print(dd)
        while dd <= date(self.sim.context.year, 12, 31):
            care = DrugDelivery(cip, patient, self.sim.pharms[0]) #TODO: select another pharmacy
            care.date_debut = dd
            care.date_fin = dd
            patient.drugdeliveries.append(care)
            print(f"      -> inject at {dd}!")

            dd = dd + relativedelta(months=+1)

        # add a consultation (1111) of specialists (32: neurologue)
        # -> is there already a neurolog among specialists
        spes = [spe for spe in self.sim.specialists if spe.speciality==32]
        if len(spes)!=0 :
            care = MedicalVisit(patient, spes[0])
            care.date_debut = dd + relativedelta(months=1, days= int(random.integers(0,10)))
            care.date_fin = care.date_debut
            patient.visits.append(care)
            print(f"      -> inject visit neuro!")

        # add a consultation (1111) of specialists (3: cardio-vasculaire)
        spes = [spe for spe in self.sim.specialists if spe.speciality==3]
        if len(spes)!=0 :
            care = MedicalVisit(patient, spes[0])
            care.date_debut = dd + relativedelta(months=3, days= int(random.integers(0,10)))
            care.date_fin = care.date_debut
            patient.visits.append(care)
            print(f"      -> inject visit cardio!")
        # -> is there already a neurolog among the specialists

    def __index_date__(self, patient: Patient):
        # look for an hospital stay with a diagnosis for 'AVC'
        # the leave date gives the beginning of the pathway
        for stay in patient.hospitalStays:
            if stay.DP in ("I60", "I61", "I62", "I63", "I64"):
                return stay.finish_date
        return pd.NaT



class GenEpiPathwayInjector(pathwayInjector):
    def __init__(self, sim: simulation):
        super().__init__(sim)

        cur = self.sim.context.conn.cursor()
        cur.execute("select distinct PHA_ATC_C07, PHA_CIP_C13 from IR_PHA_R where PHA_ATC_C07 in ('N03AF01', 'N03AX09', 'N03AX14', 'N03AX11', 'N03AG01', 'N03AF02');")
        codes_medoc_q = cur.fetchall()
        cur.close()

        #creation of a dictionary of CIP codes organized by ATC codes
        self.codes_medoc={}
        for c in codes_medoc_q:
            if c[0] not in self.codes_medoc:
                self.codes_medoc[c[0]]= [c[1]]
            else:
                self.codes_medoc[c[0]].append( c[1] )

    def pathdesc(self, patient: Patient, d: date):
        """hide a switch that can be investigated/retrieved with a case-control protocol."""

        if len(self.sim.pharms) == 0:
            warnings.warn(
                "Warning (GenEpiPathway): no pharmacies available to create the pathway"
            )
            return

        random = rd.default_rng()

        # tirer aléatoirement si switch case et si switch control
        switch_case= (random.uniform(0,1)>0.6)
        switch_control= (random.uniform(0,1)>0.4)

        if not switch_case and not switch_control:
            # no switch
            class_atc=random.choice(list(self.codes_medoc.keys()))

            dd = date(self.sim.context.year, 1, 1+int(random.uniform(0,27)))
            while dd <= date(self.sim.context.year, 12, 31):
                cip = random.choice( self.codes_medoc[class_atc] )
                care = DrugDelivery(cip, patient, self.sim.pharms[0])
                care.date_debut = dd
                care.date_fin = dd
                patient.drugdeliveries.append(care)
                #print(f"      -> inject at {dd}!")
                dd = dd + relativedelta(months=+1)
        elif switch_case and not switch_control:
            # choose the switch month in the switch case
            d_switch = d + relativedelta(days= -int(random.integers(3,93)))
            # choose the two atc alternatives
            class_atc=random.choice(list(self.codes_medoc.keys()), 2, replace=False)

            dd = date(self.sim.context.year, 1, d_switch.day)
            while dd <= date(self.sim.context.year, 12, 31):
                if dd < d_switch:
                    cip = random.choice( self.codes_medoc[ class_atc[0] ] )
                else:
                    cip = random.choice( self.codes_medoc[ class_atc[1] ] )
                care = DrugDelivery(cip, patient, self.sim.pharms[0])
                care.date_debut = dd
                care.date_fin = dd
                patient.drugdeliveries.append(care)
                #print(f"      -> inject at {dd}!")
                dd = dd + relativedelta(months=+1)
        elif not switch_case and switch_control:
            # choose the switch month in the switch case
            d_switch = d + relativedelta(days= -int(random.integers(93,183)))
            # choose the two atc alternatives
            class_atc=random.choice(list(self.codes_medoc.keys()), 2, replace=False)

            dd = date(self.sim.context.year, 1, d_switch.day)
            while dd <= date(self.sim.context.year, 12, 31):
                if dd < d_switch:
                    cip = random.choice( self.codes_medoc[ class_atc[0] ] )
                else:
                    cip = random.choice( self.codes_medoc[ class_atc[1] ] )
                care = DrugDelivery(cip, patient, self.sim.pharms[0])
                care.date_debut = dd
                care.date_fin = dd
                patient.drugdeliveries.append(care)
                #print(f"      -> inject at {dd}!")
                dd = dd + relativedelta(months=+1)
        else:
            # two switches
            # choose the switch month in the switch case
            d_switch_case = d + relativedelta(days= -int(random.integers(93,183)))
            d_switch_control = d + relativedelta(days= -int(random.integers(3,93)))
            # choose the two atc alternatives
            class_atc=random.choice(list(self.codes_medoc.keys()), 3, replace=False)

            dd = date(self.sim.context.year, 1, d_switch_case.day)
            while dd <= date(self.sim.context.year, 12, 31):
                if dd < d_switch_control:
                    cip = random.choice( self.codes_medoc[ class_atc[0] ] )
                elif dd >= d_switch_control and dd < d_switch_case:
                    cip = random.choice( self.codes_medoc[ class_atc[1] ] )
                else:
                    cip = random.choice( self.codes_medoc[ class_atc[2] ] )
                care = DrugDelivery(cip, patient, self.sim.pharms[0])
                care.date_debut = dd
                care.date_fin = dd
                patient.drugdeliveries.append(care)
                #print(f"      -> inject at {dd}!")
                dd = dd + relativedelta(months=+1)


        # add a consultation (1111) of specialists (32: neurologue) during the year
        spes = [spe for spe in self.sim.specialists if spe.speciality==32]
        if len(spes)!=0 :
            care = MedicalVisit(patient, spes[0])
            care.date_debut = date(self.sim.context.year, 1, 1) + relativedelta(days= int(random.integers(0,364)))
            care.date_fin = care.date_debut
            patient.visits.append(care)
            print(f"      -> inject visit neuro!")
        # -> is there already a neurolog among the specialists

    def __index_date__(self, patient: Patient):
        # look for an hospital stay with a diagnosis for 'epileptic crisis'
        # the leave date gives the beginning of the pathway
        for stay in patient.hospitalStays:
            if stay.DP in ("G40", "G41"):
                return stay.finish_date
        return pd.NaT
