import numpy as np
from matplotlib import pyplot as plt
from scipy.stats import exponweib
from scipy.optimize import fmin

# x is your data array
# returns [shape, scale]
def fitweibull(x):
    def optfun(theta):
        return -np.sum(np.log(exponweib.pdf(x, 1, theta[0], scale = theta[1], loc = 0)))
    logx = np.log(x)
    shape = 1.2 / np.std(logx)
    scale = np.exp(np.mean(logx) + (0.572 / shape))
    return fmin(optfun, [shape, scale], xtol = 0.01, ftol = 0.01, disp = 0)

# x is your data array
# returns [shape, scale]
# https://www.real-statistics.com/distribution-fitting/distribution-fitting-via-maximum-likelihood/weibull-censored-data/
def fitweibull_censored(x, c):
    """
    Fit the parameters of a Weibull distribution assuming that the data are right-censored by c
    https://www.real-statistics.com/distribution-fitting/distribution-fitting-via-maximum-likelihood/weibull-censored-data/

    x is your data array, all non-null elements are in [0, c], null values are interpreted as censored values 
    c censor threshold
    returns [shape, scale]
    """

    #compute the number of censored elements (NaN or above c)
    nb_censored = np.isnan(x).sum() + (x>c).sum()
    #use only the values that are not censored
    x_uncensored = x[ (~np.isnan(x)) & (x<c) ]

    def optfun(theta):
        #theta[0]: k, theta[0]: lambda

        # loglikelihood for uncensored data
        LL = -np.sum(np.log(exponweib.pdf(x_uncensored, 1, theta[0], scale = theta[1], loc = 0)))
        # + loglikelihood for censored data
        LL += -nb_censored * (1- exponweib.cdf(c, 1, theta[0], scale = theta[1], loc = 0))
        return LL
    logx = np.log(x_uncensored)
    shape = 1.2 / np.std(logx)
    scale = np.exp(np.mean(logx) + (0.572 / shape))
    return fmin(optfun, [shape, scale], xtol = 0.01, ftol = 0.01, disp = 0)


k = 0.25732
lbda =  28071.5399
print(k,lbda)

rng = np.random.default_rng()
x=lbda*rng.weibull(k,size=1000)

x[x>365]=np.NaN

k_fw,lbda_fw=fitweibull(x[ ~np.isnan(x)])
print(k_fw,lbda_fw)


k_cfw,lbda_cfw=fitweibull_censored(x, 365)
print(k_cfw,lbda_cfw)

"""
x=x[x<365]

xp = np.linspace(0, 365, 1000)
yp = (k/lbda)*np.power((xp/lbda), k-1)*np.exp( - np.power(xp/lbda,k) )
plt.plot(xp, yp)
plt.plot(x,np.random.uniform(0.09,.1,len(x)), 'g^')
plt.show()
"""
