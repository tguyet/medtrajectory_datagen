#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This script illustrates how to generate efficiently a dataset made of patients, their ALD status and drug deliveries (without timestamps)

@author: Thomas Guyet, Inria
@date 21/2/2022
"""

import pandas as pd
import numpy as np


#### GENERATION of a POPULATION

taille_pop=1000

##Generate a population
pop=pd.read_csv("../datarep/pop.csv").drop(['Unnamed: 0'],axis=1)
pop=pop.dropna()
tot_pop = np.sum(pop['pop'])
pop['pop']/=tot_pop

population=pop.iloc[np.random.choice(len(pop['pop']),p=pop['pop'],size=taille_pop)].drop(['pop'],axis=1).reset_index(drop=True)
population

######## Assign ALD to patients

Pald=pd.read_csv("../datarep/ALD_p.csv").drop(['Unnamed: 0'],axis=1)
#same proba everywhere (to simplify)
Pald=Pald.groupby(['sex','age','ALD']).mean('p').reset_index()

ald=pd.merge( population, Pald.pivot(index=['sex','age'],columns='ALD',values='p').reset_index(), on=["sex","age"]).drop(['age','sex','dpt','code','city_name','RR'],axis=1)
for i in range(30):
    ald['ALD'+str(i+1)]=(ald['ALD'+str(i+1)]>np.random.rand(len(ald)))

###### GENERATION of Drug Deliveries

#Generate some random drug deliveries
drugs_freq=pd.read_csv("../datarep/drugs_freq.csv").drop(['Unnamed: 0'],axis=1)
nb_deliveries = pd.read_csv("../datarep/mean_deliveries.csv").drop(['Unnamed: 0'],axis=1)

# generation to do
to_generate=population[['age','sex','RR']].groupby(['age','sex','RR']).size().reset_index().rename(columns={0:"nb"})
to_generate=pd.merge(to_generate, nb_deliveries, on=['age','sex','RR'])
to_generate['nb']=np.round(to_generate['nb']*to_generate['mean'])
to_generate.drop('mean', axis=1, inplace=True)
to_generate=to_generate[to_generate['nb']!=0]


#for a given set of characteristics to generate drugs
def generate_drugs(x):
    age=x['age']
    sex=x['sex']
    RR=x['RR']
    nb=int(x['nb'])
    
    possible_drugs=drugs_freq[(drugs_freq['age']==age) & (drugs_freq['sex']==sex) & (drugs_freq['RR']==RR)][['CIP13','p']]
    if len(possible_drugs)==0:
        return None
    possible_drugs['p']/=np.sum(possible_drugs['p'])
    
    loc_drugs=possible_drugs.iloc[np.random.choice(len(possible_drugs),p=possible_drugs['p'],size=nb)].drop(['p'],axis=1).reset_index(drop=True)
    
    #list of the people having the characteristics
    indices=population[(population['age']==age) & (population['sex']==sex) & (population['RR']==RR)].reset_index()['index']
    loc_drugs['patient']=np.random.choice(indices, size=len(loc_drugs))
    return loc_drugs

res=to_generate.apply(generate_drugs, axis=1)
drugs=pd.concat(res.to_list())

#simu={'patients':population, 'drugs' : drugs, 'ALD': ald}
###################

## Now ... we generate a database using these datasets

from data_factory import PharmacyFactory, DrugsDeliveryFactory
from data_factory import FactoryContext,PatientFactory

# create a context with the default nomenclature DB
context =  FactoryContext()

#generate the patients 
factory = PatientFactory(context)
attribute_map={'sex':'sex', 'age':'age'}
patients=factory.load(population, attribute_map)

#generate the drug deliveries
drugs_attributes={'pid':'patient','cip':'CIP13'}

pfactory=PharmacyFactory(context)
pharms = pfactory.generate(10)

drugfact=DrugsDeliveryFactory(context, pharms)

drugfact.load(patients[0],drugs, drugs_attributes)

#add drug deliveries to patients !
drugfact.load_allpatients(patients,drugs, drugs_attributes)


from simuExternal import simuExternal

# representation of a simulation with datasets
simu = {
        "patients":{
                "data":population,
                "attributes":{'sex':'sex', 'age':'age'}
                },
        
        "drugs":{
                "data":drugs,
                "attributes":{'pid':'patient','cip':'CIP13'}
                }
        }


simulator = simuExternal(nomencl="../datarep/snds_nomenclature.db",
                 datarep="../datarep")
simulator.run()
simulator.load(simu)

from simulationDB import simDB
dbgen = simDB()
dbgen.generate(simulator, rootschemas="../schema-snds/schemas")
