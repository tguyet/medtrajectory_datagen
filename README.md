# SNDS synthetic data generator

The SNDS (_Système National des Données de Santé_), formerly SNIIRAM, is a huge database (several Tb of data and about 700 tables) that contains information about healthcare reimbursements of about 60 million French insured patients.

This database is used to carry out epidemiological and medical-economic studies. Due to its sensitive medical content, it is hard to access this data and thus to develop machine learning tools based on this data. 

Then, our objective is to provide synthetic data conforming the schema of the original database (the version proposed by the Health Data Hub more specifically) while keeping some realism in the data to make it more realistic. 
The expected usage of generated databases are:
1. to test technically a code based on SNDS databases,
2. for preparing courses in epidemiology with scenario of realistic data (see for instance the following [course](https://gitlab.inria.fr/tguyet/Cours_EINS) )
3. for evaluating machine learning tools on synthetic complex data

The spirit of the generator is to generate "privacy"-safe data ... and it does not require real data to generate realistic synthetic data. It is based on two modeling entities:
- intermediary distributions of events (drug deliveries, medical visits, hospitalization, etc.) that are prepared from populational public datasets (not individual data) 
- expert care pathways that are provided by expert knowledge

Both entities are used to define the characteristics of a population to reproduce in the synthetic dataset: the populational distribution of some events and some care pathways. 
It leads to a partially realistic synthetic data, that fit our purposes above.

To sum up, this repository contains a solution to generate a synthetic version of the SNDS database:
1. it generates relational data compliant with the original database schema, 
2. it generates data with realistic distributions, 
3. it guarantees privacy preservation thanks to the use of open data only.

An example of output and some experiments can be found in the following notebook (accessible directly within the online GitLab visualisation: [notebook](/Generator/PostGenerationAnalysis_Bretagne.ipynb) ).



## More details about the generation principles

The main objective of the project is to simulate a synthetic dataset of care pathways that may have some (statistical) similarities with a real dataset and that has some privacy guarantees.
More especially, we focus on the SNDS database schema.

In order to acheive this goal, we propose to organize the project in 3 main steps:
  1. reproduce simple global statistical features such as the distribution of medics in the whole population, distribution of genders, distribution of physicians, geographic distributions, etc. Here, the specificy is to reproduce aggregated statistics that are available in open data. The challenge here is to have statistical processes to would approxiate the joint distributions!
  2. reproduce some characteristics of care pathways. At this stage, the objective is to reproduce some count statistics at the individual level. This objective requierts to have an access to real care pathways from which extract the statistical characteristics to reproduce.
  3. generate realistic care pathways (and collection of patients with different types of pathways), including the sequentiality and the delays between cares. Statistical random generation processes may be blended with rule based generation processes to reproduce care trajectories.


### Modeling principle

The simulator is based on an internal model of the french health system. This model represents the entities that are then reified in the database.

To generate data in the schema of the SNDS, our model considers different aspects of the health system:
* the *medical environment*: it represents the types of cares or drugs that can be delivered, the geography, the temporal bourdaries of the simulation. All this information can be considered as static and available in the nomenclaure
* the *care offer*: it represents the set of care providers (physicians, pharmacists, GPs, specialists, hospital...). The list of hospital in known, but the list of care provider has to be randomly generated
* the *population*: it represents the set of patients to which cares have been delivered (or not). 
* the *care pathways*: it represent the sequence of cares delivered for a patient

For each of these aspects, there are specific data to reuse or to generate.

From the computer enginering point of view, the main idea of the tool is to have two different layers:
* a theoretical model of a popution/environment/cares: it is an internal model that we start to organize be listing the large four categories of entities
* a concrete data generation model that is able to concretise an instance of the model in a data format. The first target is a SNDS database (we choose a SQLite database and the database schema provided by the HDH). The second target is a RDF schema.


The theoretical model is encoded as an object oriented model.

### A object oriented model of the French health system

The model is given in the `database_model.py` file which describes different classes that represent the entities of our model.
The `data_factory.py` file holds classes that are factories of the classes in the model. This factories may be implemented with the different strastegies we introduced above (from random to realistic).

The model we propose is made of the following main classes:
* `PS` a care provider that has sub-classes
    * `Provider` that, at the time, represents the pharmacists
    * `Physician`  that can be a `GP` or a `Specialist`
* `Patient` 
* `CareDelivery` that corresponds to the different types of care a patient may receive (drug deliveries, medical acts, GP visits, hospital stays, etc.). The categories of cares are guided by the SNDS stucture that model outpatient trajectories.
    * `DrugDelivery`
    * `Visits`
    * `ShortStays`


## How to??

This section presents more technical details about how to use the code in practice.
Please note that it has been tested with a Linux system. 

### Organisation of this repository

This repository contains different parts:

```sh
├── data
├── Data_Preparation
├── Models
│   ├── AVC
│   ├── GeneralPop
│   └── VICAN
├── SNDSGenerator
└── snds_sqltordf
```

* `data` contains some resources data useful to create a database mimicking some real characteristics of the SNDS. 
* `Data_Preparation` contains, on the one side, bash and python scripts used to set up files needed by the generator. On the other side, it contains a set of notebooks that details how the random distributions have been generated from the raw-public datasets
* `Models` contains specific instanciations of the generator. Currently, we propose three different bases: AVC, GeneralPop and VICAN
* `SNDSGenerator` contains the generator engine
* `snds_sqltordf` contains scripts and ressources to generate a RDF version of SNDS


### Installation 

This tools has been implemented and tested with **Python 3.9**, and with specific versions of the libraries. We highly recommand to use the installation procedure below to ensure an easy installation.

The software is developed using python tools (script and notebooks). It used the following specific libraries: 
- sqlalchemy
- sqlite3
- tableschema
- tableschema-sql
- wget
- unzip
- gzip
- pandas
- jupyterlab

This project does not use the last version of the libraries! **You need to install the exact environment** suggested in the `requirements.txt` file. More specifically:
  * `Tableschema` is not compatible with SQLAlchemy 2, it has been replaced by `frictionless` (but not sure it is more compatible with recent version of SQLAlchemy)
  * the version of `sqlalchemy` is also not the most recent one


We highly recommand to proceed as follows (with a virtual environment)

```bash
git clone --recursive git@gitlab.inria.fr:tguyet/medtrajectory_datagen.git
cd medtrajectory_datagen
python3.9 -m venv localenv
source localenv/bin/activate
pip install -r requirements.txt
```

Following this instruction must lead to arepository ready for the generation of a synthetic database.

### How to generate your own database

Before being able to generate new synthetic databases, you need to prepare a collection of file (mainly containing theoretical distributions of the population with their gender and age, but also some global medical information gathered from public databases).

To generate your first synthetic database, we suggest to proceed as follows:
1. _If you did not clone recursively the repo_, you will need to download the database schema that is provided by the Health Data Hub (otherwise, skip this step)
```bash
git clone https://gitlab.com/healthdatahub/schema-snds.git
```
2. Create the core database (with nomenclatures)
 * Run the script `prepare.py` that cleans the SNDS schemas (fix some typos, etc.)
 ```bash
 python Data_Preparation/prepare.py
 ```
 * Run the script `create_nomenclature.py` to create the nomenclature part of the SNDS. This script create a large database made of the SNDS tables that can be provided without access restriction. This part of the database is provided by the Health Data Hub.
 ```bash
 python Data_Preparation/create_nomenclatures.py
 mkdir datarep
 mv snds_nomenclature.db datarep/
 ```
3. Retrieve the populational distributions of events. There are two alternatives for that (the first one is highly recommended):
  * Retrieve the pre-computed distribution
```bash
wget https://zenodo.org/records/12704189/files/populational_distributions.zip
unzip populational_distributions.zip -d datarep/
```
  * Alternatively, you can regenerate the distributions from the raw open data 
    1. Download all the raw data available online
      * In the `data` repository, run the script `load_opendata.py` to download and unzip open data in the repository
      * Some open data links may require user interactions to start the download, in this case, datasets have to be manually downloaded (use the links that are in the `load_opendata.py` script)
    2. Generate intermediary files (distributions that will be reproduced)
      * Start by running the script `prepare_data.py` (ensure all the flags at the beginning of the script are set to `True` to generate all the required files)
      * Each step is detailed in corresponding Notebooks in the `Data_Analysis` repository (which explains the details of the computation and draw Figures to assess the approximations)
4. Run the `generate_avcdataset.py` script to run the simulator based on Open Data. This script can be modified to setup some parameters if the simulation, for instance: the population size, the number of physicians, the administrative regions to mimic, etc.
```bash
python Models/AVC/generate_avcdataset.py
```
5. Then ... you have a database in the `AVCGenerator` directory that can be browsed. For instance:
```bash
sqlitebrowser Models/AVC/snds_testgen.db &
```

## Open data resources 

In this section, we give the exhaustive list of the open datasets we are currently using to feed realistically our database. Please note that some of them may have change location. This is the reason why we provide intermediary files that is the result of a preprocessing of these files.

* SNDS data originates from the Synthetic SNDS project [https://gitlab.com/healthdatahub/synthetic-snds]
 - SNDS Schemas have to be downloaded on the HdH [https://gitlab.com/healthdatahub/schema-snds]
  * The schemas use the `Table Schema` format
  * WARNING: I add to make small changes in the schemas to make it compatible with SQite
 - SNDS Nomenclature datasets (also available in the repos above)
  * CSV dataset containing the nomeclature used in the SNDS database
  * this information is anonymous
    
* Data coming from national statistics institute
  - population statistics, 2016: Quinquenal age, sex, city population [https://www.insee.fr/fr/statistiques/1893204#consulter]
  
* Data coming from the French Health Insurance (most of them are aggregated data from the raw SNDS itself)
 - For drugs deliveries Open Medic 2019, download: [https://www.data.gouv.fr/fr/datasets/r/91d193b4-dd34-4cda-a843-bd1eb30aa7bc]
 - For medical acts (outhospital), Open Damir 2019, download: [https://www.data.gouv.fr/fr/datasets/depenses-d-assurance-maladie-hors-prestations-hospitalieres-par-caisse-primaire-departement/] (used only data in January due to the size of the data)
  - Open CCAM [https://www.scansante.fr/open-ccam/open-ccam-2016]: contains CCAM codes (not OpenDamir)
 - Information about Care Providers, Open data: [https://www.data.gouv.fr/fr/datasets/annuaire-sante-de-la-cnam/], download: [https://www.data.gouv.fr/fr/datasets/r/296394b6-d539-4cc7-a440-2698eec06c18]
 - Data about ALD (Long cares) 2016, Open data: [https://www.data.gouv.fr/fr/datasets/personnes-en-affection-de-longue-duree-ald/], downloads
  * per age/sex  [https://www.data.gouv.fr/fr/datasets/r/360e6600-7c05-46ef-aac5-cf0ea84968ae]
  * per dpt [https://www.data.gouv.fr/fr/datasets/r/26ac640b-e65f-4324-bbed-e0e9690bf449]
  * data have been reorganized manually
 - Data about care etablishment (including hospitals, pharmacies) FINESS database [https://www.data.gouv.fr/fr/datasets/finess-extraction-du-fichier-des-etablissements/], download: [https://www.data.gouv.fr/fr/datasets/r/16ee2cd3-b9fe-459e-8a57-46e03ba3adbd]
  * Note that the FINESS database is also available in the nomenclature
  * I used the clean version of the FINESS dataset, cleaned by a user (`finess_clean.csv`): [https://www.data.gouv.fr/fr/datasets/r/3dc9b1d5-0157-440d-a7b5-c894fcfdfd45]
 - Informations about hospitalisation (numbers of hospitalisation, CIMS codes) have been digged from [https://www.scansante.fr/] and [http://www.aideaucodage.fr/]
 - Data about deaths comes from [https://www.data.gouv.fr/fr/datasets/deces-par-cause/]

We invite the reader to have a look at the data preparation notebooks to have a flavour of the content of these datasets.


## Questions and Issues
If you find any bugs or have any questions about this code please contact Maximilian. We cannot guarantee any support for this software.

## How to cite

Please cite our paper if you use this code in your research. 
You can cite the following [article](https://hal.science/hal-03326618/file/datasynth.pdf) that presents the first version of the generator.

```bibtex
@inproceedings{guyet:hal-03326618,
  TITLE = {An open generator of synthetic administrative healthcare databases},
  AUTHOR = {Guyet, Thomas and Allard, Tristan and Bakalara, Johanne and Dameron, Olivier},
  URL = {https://hal.science/hal-03326618},
  BOOKTITLE = {{IAS} - Atelier Intelligence Artificielle et Sant{\'e}},
  ADDRESS = {Bordeaux (virtuel), France},
  PAGES = {1-8},
  YEAR = {2021}
}
```

## Authors

* [Thomas Guyet](http://thomas.guyet.info/), Inria/AIstroSight